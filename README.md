![popcom logo](./popcom_logo.png)

# Commission on Population Data Management System
Commission on Population is one of the health agencies mandated to serve as the Central coordinating and policy making body of the government field of population. It was officially launched through the Executive Order No. 233.

#### Their Vision
Responsible individuals, well-planned, prosperous healthy and happy families, empowered communities, guided by the Divine Providence living harmoniously and equitably in a sustainable environment.


#### Their Mission
We are a technical and information resource agency, working in partnership with national and local government policy and decision makers, program implementers, community leaders and civil society.

#### System Design Outline
1. Unified interface for all regions (RPFP, AHD, POPDEV).
1. Multiple-area data management support (RPFP,AHD, POPDEV).
1. Indicator-based matrix dynamically changes based on the active area. (RPFP, AHD, POPDEV).
1. Data is centralized and is available to each area in the region.
1. Data sources are based on the given data(PSA, PopCom, DOH, ETC)
1. Login will be based on the Level of Policy indicated by the Administrator
1. Validation will be on the Percentage Format
1. Indicators can be added, edited, updated by the administrators only.
1. Source can be added, edited, updated by the administrators.
1. Chart Report may vary on the input given on the data entry.
1. Reports can be generated in PDF on the fly.

#### Development Timeframe
| Timeframe | Deliverables | Percentage | Due Date |
| ------------- |:-------------|:-----:|:-----:|
| First Release | Upon submission and acceptance of the system design of the online M&E system according to the specifications provided by POPCOM | 15% | 8/25/2017 |
| Second Release | Upon submission and acceptance of the first version of the online M&E System and receipt of the comments and recommendations of PMED |   30% | 11/15/2017 |
| Third Release | Upon submission and acceptance of the revised online M&E systems after integrating all the comments and recommendations of PMED and after the presentation to the EXECOM including receipt of the EXECOM�s comments and recommendations | 30% | 12/8-17/2017 |
| Fourth Release | Upon submission and acceptance of the final version of the online M&E System, the conduct of two orientations (technology transfer with POPCOM IT staff using the technical manual and Operationalization of the system with Central Office staff using the user manual) and the submission of technical and user manual (PDF copy and hard copy)| 25% | 12/20/2017 |

#### Project Structure
|Item|Description|
|----|:-----|
|```documents```| Contains the necessary documents needed for the development of the system.|
|```prototyping```| Includes the UI and UX design documents of the project. UI design contains projects to the Low-Fi, Medium-Fi and High-Fi designs. |
|```public```| HTML skeleton of the project |
|```src```| Project's source code |
