import React, { Component } from 'react';
import {Dropdown, Button} from 'semantic-ui-react';
import ReactFireMixin from "reactfire";
import * as firebase from 'firebase';
import _ from 'lodash';
export default class CollapsibleDropdown extends Component {
    mixins: [ReactFireMixin]
    constructor(props){
        super(props);
        this.state = {
            selection: '',
            toggled: false,
            toggleEditIcon:false,
            value: []
        }
    }

    handleChange = (e, { name, value, text }) => {        
        // this.props.firebaseRef.update({[this.props.childKey]:value}).then(()=>{
        //     this.setState({ value: value, selection: _.startCase(value), toggled: false });
        // });
        this.setState({ value: value, selection: _.startCase(value), toggled: false });     
    };

    componentDidMount(){
        this.setState({selection:this.props.selection})
    }

    toggleComponent = ()=> {
        this.setState({toggled:!this.state.toggled});
        if(this.state.toggled==false){
            this.setState({toggleEditIcon:false})
        }
    }

    toggleEditIcon = ()=>{
        this.setState({toggleEditIcon: !this.state.toggleEditIcon})
    }

    render(){
        const {selection, toggled, toggleEditIcon} = this.state;

        return(
            <div>
                {toggled?<Dropdown {...this.props} onChange={this.handleChange} onClose={this.toggleComponent}/>:                         
                    <Button icon={toggleEditIcon?'pencil':''} style={{textAlign: 'left', backgroundColor:'transparent'}} compact onClick={this.toggleComponent} onMouseEnter={this.toggleEditIcon} onMouseLeave={this.toggleEditIcon} content={selection} />
        
                }
            </div>
        )
    }
}