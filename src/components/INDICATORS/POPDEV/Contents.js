import React, { Component } from 'react';
import {Container, Progress, Menu, Header, Tab} from 'semantic-ui-react'
import * as firebase from 'firebase';

import OutcomeIndicators from './OutcomeIndicators';
import ImpactIndicators from './ImpactIndicators';
import InputIndicators from './InputIndicators';
import OutputIndicators from './OutputIndicators';

const panes = [
    { menuItem: 'Impact', render: () => <Tab.Pane><ImpactIndicators /></Tab.Pane> },
    { menuItem: 'Outcome', render: () => <Tab.Pane><OutcomeIndicators /></Tab.Pane> },
    { menuItem: 'Input', render: () => <Tab.Pane><InputIndicators /></Tab.Pane> },
    { menuItem: 'Output', render: () => <Tab.Pane><OutputIndicators /></Tab.Pane> },   
]

export default class Contents extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>          
                <Container>
                    <Header content="POPDEV Indications" />
                    <Tab panes={panes} />
                </Container>
            </div>
        )
    }
}