import React, { Component } from 'react';
import {Form, Table} from 'semantic-ui-react'
import * as firebase from 'firebase';
const uuidv4 = require('uuid/v4');
const sourceOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'psa', value: 1, text: 'PSA' },
    { key: 'popcom', value: 2, text: 'POPCOM' },
    { key: 'doh', value: 3, text: 'DOH' },
    { key: 'dswd', value: 4, text: 'DSWD' },
]

const attendeesOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'male', value: 1, text: 'Male' },
    { key: 'female', value: 2, text: 'Female' },
    { key: 'couples', value: 3, text: 'Couples' }
]

const indicatorPath = "output";

export default class rpfpInputIndicators extends Component {
    constructor(props) {
        super(props);
        this.indicatorsRef = firebase.database().ref("indicators").child("template");
        this.currentUser = firebase.auth().currentUser;
        this.state = {
            region: {
                regCode: 0
            },
            data: [],
            loading: true
        }
    }

    componentDidMount() {
        const key = 'rpfp_'+indicatorPath+'_indicators';

        let offlineData = localStorage.getItem(key);

        if (offlineData !== null) {
            this.setState({ loading: false, data: JSON.parse(offlineData) })
        }

        this.indicatorsRef.child("rpfp").child(indicatorPath).on("value", d => {
            localStorage.setItem(key, JSON.stringify(d.val()))
            this.setState({ data: d.val(), loading: false })
        })
    }

    render() {
        const { data, loading, region } = this.state;
        return (
            <div>
                <Form loading={loading}>
                    <Table celled striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Indicators</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {data !== [] && data!==null && Object.keys(data).map(key => {
                                return (
                                    <Table.Row key={uuidv4()}>
                                        <Table.Cell>
                                            {data[key].name}
                                        </Table.Cell>
                                    </Table.Row>
                                )
                            })}
                        </Table.Body>
                    </Table>
                </Form>
            </div>
        )
    }
}
