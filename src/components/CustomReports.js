import React, { Component } from 'react';
import { Input, Message, Menu, List, Step, Image, Icon, Modal, Header, Grid, Divider, Button, Container, Checkbox } from 'semantic-ui-react'
import moment from 'moment';
import * as firebase from 'firebase';
import async from 'async';
import { Redirect } from 'react-router';
const uuidv4 = require('uuid/v4');
export default class CustomReports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region_all: false,
            reg1: false,
            reg2: false,
            reg3: false,
            reg4a: false,
            indicator_cat_all: false,
            rpfp: false,
            popdev: false,
            ahd: false,
            indicator_group_all: false,
            impact: false,
            outcome: false,
            input: false,
            output: false,
            indicator_data_all: false,
            age: false,
            attendees: false,
            source: false,
            indicators: null,
            modalOpen: false,
            query: null,
            navigate: false,
            processing:false
        }
        this.indicatorsRef = firebase.database().ref("indicators").child("template");
        this.handleChange = this.handleChange.bind(this);
        this.handleGroupChange = this.handleGroupChange.bind(this);
    }

    handleChange(e, { name, checked }) {
        const state = { [name]: checked }
        this.setState(state);
    }

    handleProcessReport = () => {
        
        const { query } = this.state;
        const app = this;
        let datastore = []
        // Reset
        localStorage.removeItem('custom_report');
        if (query != null) {
            async.forEachOf(query.regions, function (value, key, callback) {
                firebase.database().ref("data/regions/" + key).once("value").then(s => {
                    let data = s.val();
                    delete data.demand;
                    query.indicator_cat[0] === false && delete data.rpfp
                    query.indicator_cat[1] === false && delete data.popdev
                    query.indicator_cat[2] === false && delete data.ahyd

                    if (data.rpfp) {
                        query.indicator_group[0] == false && delete data.rpfp.impact
                        query.indicator_group[1] == false && delete data.rpfp.outcome
                        query.indicator_group[2] == false && delete data.rpfp.input
                        query.indicator_group[3] == false && delete data.rpfp.output
                    }

                    if (data.popdev) {
                        query.indicator_group[0] == false && delete data.popdev.impact
                        query.indicator_group[1] == false && delete data.popdev.outcome
                        query.indicator_group[2] == false && delete data.popdev.input
                        query.indicator_group[3] == false && delete data.popdev.output
                    }

                    if (data.ahyd) {
                        query.indicator_group[0] == false && delete data.ahyd.impact
                        query.indicator_group[1] == false && delete data.ahyd.outcome
                        query.indicator_group[2] == false && delete data.ahyd.input
                        query.indicator_group[3] == false && delete data.ahyd.output
                    }
                    if (value) {
                        datastore.push({region:key,data:data})
                    }
                    callback()
                })

            }, function (err) {
                if (err) {
                    console.error(err.message);
                } else {
                    app.setState({processing:true})                    
                    localStorage.setItem('custom_report', JSON.stringify(datastore));
                    setTimeout(()=>{
                        app.setState({ navigate: true })
                    }, 2000)
                    
                }
            })
        }
    }

    handleGenerateReport = () => {
        const {
            reg1,
            reg2,
            reg3,
            reg4a,
            rpfp,
            popdev,
            ahd,
            impact,
            outcome,
            input,
            output,
            age,
            attendees,
            source,
        } = this.state;
        this.setState({ modalOpen: true })
        let regions = [reg1, reg2, reg3, reg4a]
        let indicator_cat = [rpfp, popdev, ahd]
        let indicator_group = [impact, outcome, input, output]
        let indicator_data = [age, attendees, source]

        this.setState({ query: { regions: regions, indicator_cat: indicator_cat, indicator_group: indicator_group, indicator_data: indicator_data } })


        // Regions

        // Object.keys(data).map(i => {
        //     if (data[i] === true
        //         && i != "indicator_cat_all"
        //         && i != "region_all"
        //         && i != "indicator_group_all"
        //         && i != "indicator_data_all"
        //         && i != "modalOpen"
        //         && i != "query"
        //     ) {
        //         query.push(region)
        //     }
        // })
    }

    handleGroupChange(e, { name, checked }) {
        switch (name) {
            case "region_all":
                this.setState({
                    reg1: checked,
                    reg2: checked,
                    reg3: checked,
                    reg4a: checked,
                    [name]: checked
                });
                break;
            case "indicator_cat_all":
                this.setState({
                    rpfp: checked,
                    popdev: checked,
                    ahd: checked,
                    [name]: checked
                });
                break;
            case "indicator_group_all":
                this.setState({
                    impact: checked,
                    outcome: checked,
                    input: checked,
                    output: checked,
                    [name]: checked
                });
                break;
            case "indicator_data_all":
                this.setState({
                    age: checked,
                    attendees: checked,
                    source: checked,
                    [name]: checked
                });
                break;

            default:
                break;
        }
    }

    handleClose = () => {
        this.setState({ modalOpen: false })
        this.handleProcessReport()
    }

    componentDidMount() {
        let app = this;
        let indicator_bucket = []
        this.indicatorsRef.once("value").then(indicator => {
            async.forEachOf(indicator.val(), function (value, key, callback) {
                // NOTE: Replicate this to other marker methods
                indicator_bucket.push({ [key]: value })
                callback();
            }, function (err) {
                if (err) {
                    console.error(err.message);
                } else {
                    app.setState({ indicators: indicator_bucket })
                }
            });
        })
    }

    render() {
        const {
            navigate,
            region_all,
            reg1,
            reg2,
            reg3,
            reg4a,
            rpfp,
            indicator_cat_all,
            popdev,
            ahd,
            indicator_group_all,
            impact,
            outcome,
            input,
            output,
            indicator_data_all,
            age,
            attendees,
            source,
            indicators,
            processing
        } = this.state;
        if (navigate) {
            return <Redirect to="/dashboard/customreportview" push={true} />
        }
        return (
            <div>
                <Header as="h1" content="Custom Reports" />
                <p><b>Steps</b>: Comply the following requirements by checking the items below.</p>
                <Container>
                    <Step.Group ordered vertical fluid size="small">
                        <Step completed={reg1 || reg2 || reg3}>
                            <Step.Content>
                                <Step.Title>Select A Region</Step.Title>
                                <Step.Description>Choose your region. This step is required.</Step.Description>
                            </Step.Content>
                        </Step>

                        <Step completed>
                            <Step.Content>
                                <Step.Title>Pick A Date</Step.Title>
                                <Step.Description>Select a start and end date. Currenlty in testing mode.</Step.Description>
                            </Step.Content>
                        </Step>

                        <Step completed={rpfp || popdev || ahd}>
                            <Step.Content>
                                <Step.Title>Pick a Category</Step.Title>
                                <Step.Description>This step is required.</Step.Description>
                            </Step.Content>
                        </Step>

                        <Step completed={impact || outcome || input || output}>
                            <Step.Content>
                                <Step.Title>Select an Indicator Group</Step.Title>
                                <Step.Description>You must select atleast one.</Step.Description>
                            </Step.Content>
                        </Step>

                        <Step completed>
                            <Step.Content>
                                <Step.Title>Select an Indicator Data</Step.Title>
                                <Step.Description>Optional</Step.Description>
                            </Step.Content>
                        </Step>

                    </Step.Group>
                    <Modal
                    trigger={<Button
                    loading={processing} 
                    fluid
                    disabled={!((reg1 || reg2 || reg3)&&(rpfp || popdev || ahd)&&(impact || outcome || input || output))} 
                    color={!((reg1 || reg2 || reg3)&&(rpfp || popdev || ahd)&&(impact || outcome || input || output))?"yellow":"green"} 
                    content="Generate"
                    onClick={this.handleGenerateReport} />}
                    open={this.state.modalOpen}
                    onClose={this.handleClose}
                    size="large"
                >
                    <Header icon='cogs' content='For Processing' />
                    <Modal.Content>
                        <h3>The selected items will be processed by the system.</h3>                        
                        {/* {JSON.stringify(this.state.query)} */}
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='green' onClick={this.handleClose} inverted>
                            <Icon name='checkmark' /> Proceed
                        </Button>
                    </Modal.Actions>
                </Modal>
                <div className="content-spacer--small" />
                <p>Reporting Items</p>
                    <Divider />
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={4}>
                                <Header as="h3" content="By Region" />
                                <List>
                                    <List.Item><Checkbox name="region_all" onChange={this.handleGroupChange} checked={region_all} label='Select All' /></List.Item>
                                    <List.Item><Checkbox name="reg1" onChange={this.handleChange} checked={reg1} label='REGION I (ILOCOS REGION)' /></List.Item>
                                    <List.Item><Checkbox name="reg2" onChange={this.handleChange} checked={reg2} label='REGION II (CAGAYAN VALLEY)' /></List.Item>
                                    <List.Item><Checkbox name="reg3" onChange={this.handleChange} checked={reg3} label='REGION III (CENTRAL LUZON)' /></List.Item>
                                    <List.Item><Checkbox name="reg4a" onChange={this.handleChange} checked={reg4a} label='REGION IV-A (CALABARZON)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION IV-B (MIMAROPA)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION V (BICOL REGION)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION VI (WESTERN VISAYAS)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION VII (CENTRAL VISAYAS)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION VIII (EASTERN VISAYAS)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION IX (ZAMBOANGA PENINSULA)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION X (NORTHERN MINDANAO)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION XI (DAVAO REGION)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION XII (SOCCSKSARGEN)' /></List.Item>
                                    <List.Item><Checkbox disabled label='NATIONAL CAPITAL REGION (NCR)' /></List.Item>
                                    <List.Item><Checkbox disabled label='CORDILLERA ADMINISTRATIVE REGION (CAR)' /></List.Item>
                                    <List.Item><Checkbox disabled label='AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)' /></List.Item>
                                    <List.Item><Checkbox disabled label='REGION XIII (Caraga)' /></List.Item>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header as="h3" content="By Date Range" />
                                <Input fluid label="From" placeholder='From' value={moment().format("MMMM YYYY")} /><br /><Input fluid label="To" placeholder='To' value={moment().format("MMMM YYYY")} />

                                <Header as="h3" content="By Indicator Category" />
                                <List>
                                    <List.Item><Checkbox name="indicator_cat_all" onChange={this.handleGroupChange} checked={indicator_cat_all} label='Select All' /></List.Item>
                                    <List.Item><Checkbox name="rpfp" onChange={this.handleChange} checked={rpfp} label='RPFP' /></List.Item>
                                    <List.Item><Checkbox name="popdev" onChange={this.handleChange} checked={popdev} label='POPDEV' /></List.Item>
                                    <List.Item><Checkbox name="ahd" onChange={this.handleChange} checked={ahd} label='AHD' /></List.Item>
                                </List>

                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header as="h3" content="By Indicator Group" />
                                <List>
                                    <List.Item><Checkbox name="indicator_group_all" onChange={this.handleGroupChange} checked={indicator_group_all} label='Select All' /></List.Item>
                                    <List.Item><Checkbox name="impact" onChange={this.handleChange} checked={impact} label='Impact' /></List.Item>
                                    <List.Item><Checkbox name="outcome" onChange={this.handleChange} checked={outcome} label='Outcome' /></List.Item>
                                    <List.Item><Checkbox name="input" onChange={this.handleChange} checked={input} label='Input' /></List.Item>
                                    <List.Item><Checkbox name="output" onChange={this.handleChange} checked={output} label='Output' /></List.Item>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header as="h3" content="By Indicator Data " />
                                <List>
                                <List.Item><Checkbox name="value" checked label='Value' /></List.Item>
                                    <List.Item><Checkbox disabled name="indicator_data_all" onChange={this.handleGroupChange} checked={indicator_data_all} label='Select All' /></List.Item>
                                    <List.Item><Checkbox disabled name="age" onChange={this.handleChange} checked={age} label='Age' /></List.Item>
                                    <List.Item><Checkbox disabled name="attendees" onChange={this.handleChange} checked={attendees} label='Attendees' /></List.Item>
                                    <List.Item><Checkbox disabled name="source" onChange={this.handleChange} checked={source} label='Data Source' /></List.Item>
                                    
                                </List>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Header as="h3" content="By Indicator Item (Preview Only)" />
                    <Divider />
                    <p>Activate an indicator category to view.</p>
                    <List>
                        {indicators !== null && ahd && indicators.map(i => {
                            if (i["ahyd"] !== undefined && i["ahyd"]) {
                                return i["ahyd"].impact.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && ahd && indicators.map(i => {
                            if (i["ahyd"] !== undefined && i["ahyd"]) {
                                return i["ahyd"].input.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && ahd && indicators.map(i => {
                            if (i["ahyd"] !== undefined && i["ahyd"]) {
                                return i["ahyd"].output.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && ahd && indicators.map(i => {
                            if (i["ahyd"] !== undefined && i["ahyd"]) {
                                return i["ahyd"].outcome.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && rpfp && indicators.map(i => {
                            if (i["rpfp"] !== undefined && i["rpfp"]) {
                                return Object.keys(i["rpfp"].impact).map(key => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={i["rpfp"].impact[key].name} /></List.Item>
                                ))
                            }
                        })} {/*TODO: Replicate to others*/}

                        {indicators !== null && rpfp && indicators.map(i => {
                            if (i["rpfp"] !== undefined && i["rpfp"]) {
                                return i["rpfp"].input.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && rpfp && indicators.map(i => {
                            if (i["rpfp"] !== undefined && i["rpfp"]) {
                                return i["rpfp"].output.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && rpfp && indicators.map(i => {
                            if (i["rpfp"] !== undefined && i["rpfp"]) {
                                return i["rpfp"].outcome.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}
                        
                        {/* NOTE
                        Follow this implementation to the rest of the code to avoid key bugs */}
                        {indicators !== null && popdev && Object.keys(indicators).map(i => {
                            let indicator = indicators[i].popdev
                            if ( indicator !== undefined && indicator) {
                                let impact_indicators = indicator.impact                                                               
                                return Object.keys(impact_indicators).map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={impact_indicators[ia].name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && popdev && indicators.map(i => {
                            if (i["popdev"] !== undefined && i["popdev"]) {
                                return i["popdev"].input.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}
                        
                        {indicators !== null && popdev && Object.keys(indicators).map(i => {
                            if (indicators[i]["popdev"] !== undefined && indicators[i]["popdev"]) {
                                return indicators[i]["popdev"].output.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                        {indicators !== null && popdev && indicators.map(i => {
                            if (i["popdev"] !== undefined && i["popdev"]) {
                                return i["popdev"].outcome.map(ia => (
                                    <List.Item key={uuidv4()}><Checkbox disabled label={ia.name} /></List.Item>
                                ))
                            }
                        })}

                    </List>
                </Container>
                <div className="content-spacer--small" />
            </div >
        )
    }
}