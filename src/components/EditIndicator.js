import React, { Component } from 'react';
import {Dropdown, Button, Header, Modal, Input } from 'semantic-ui-react'
import * as firebase from 'firebase';

//firebase.initializeApp(firebaseConfig);

const sourceOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'psa', value: 1, text: 'PSA' },
    { key: 'popcom', value: 2, text: 'POPCOM' },
    { key: 'doh', value: 3, text: 'DOH' },
    { key: 'dswd', value: 4, text: 'DSWD' },
]

const attendeesOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'male', value: 1, text: 'Male' },
    { key: 'female', value: 2, text: 'Female' },
    { key: 'couples', value: 3, text: 'Couples' }
]

export default class EditIndicator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: 0,
            attendees: 0,
            remarks: "Loading...",
            validation: 0,
            loading:false,
            age:0,
            value:0
        }
        this.handleChange = this.handleChange.bind(this);
        this.ref = firebase.database().ref(this.props.path);
    }
    handleChange(e, { name, value }) {
        const state = { [name]: value }
        this.setState(state);
    }
    state = { modalOpen: false }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    handleSave = () => {
        const {source, attendees, remarks, validation, value, age} = this.state;
        if(this.modal){
            this.setState({loading:true})
        }
        this.ref.update({
            source: source,
            attendees: attendees,
            remarks: remarks,
            validation: validation,
            age: age,
            value:value,
            timestamp: firebase.database.ServerValue.TIMESTAMP
        }).then(()=>{
            if(this.modal){
                this.setState({loading:false})
            }
        })
    }

    componentDidMount(){
        this.ref.once("value").then(s=>{
            if(s.val()!==null){
                if(this.modal){
                    this.setState(s.val())
                }
            }
        })
    }

    render() {
        const {source, attendees, remarks, validation, value, age} = this.state;
        return (
            <Modal
                ref={(modal) => { this.modal = modal; }}
                trigger={<Button fluid onClick={this.handleOpen} basic icon="pencil"></Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='mini'
            >
                <Header icon='pencil' content={this.props.indicator} />
                <Modal.Content>
                    <p>Data Source</p>
                    <Dropdown name="source" placeholder='Select Source' value={source} onChange={this.handleChange} search selection options={sourceOptions} />
                    <br /><br />
                    {/* <p>Validation %</p>
                    <Input name="validation" type="number" value={validation} placeholder='0' onChange={this.handleChange}/>
                    <br /><br /> */}
                    <p>Age</p>
                    <Input name="age" type="number" value={age} placeholder='0' onChange={this.handleChange}/>
                    <br /><br />
                    <p>Type of Attendees</p>
                    <Dropdown name="attendees" placeholder='Select' value={attendees} search selection options={attendeesOptions} onChange={this.handleChange}/>
                    <br /><br />
                    <p>Value</p>
                    <Input name="value" type="number" placeholder='0' value={value} onChange={this.handleChange}/>
                    <br /><br />
                    <p>Remarks</p>
                    <Input name="remarks" placeholder='Remarks' value={remarks} onChange={this.handleChange}/>
                </Modal.Content>
                <Modal.Actions>
                <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button color='green' onClick={this.handleSave}>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}