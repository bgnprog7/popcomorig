import React, { Component } from 'react';
import {
    Button,
    Icon,
    Header,
    Menu,
    Container,
    Breadcrumb
} from 'semantic-ui-react';
import * as firebase from 'firebase';
import _ from 'lodash';
import async from 'async';
import { Doughnut, Bar } from 'react-chartjs-2';

import {
    Link,
    Redirect
} from 'react-router-dom';

let regions = ['I', 'II', 'III', 'IV-A', 'IV-B', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'ARMM', 'Caraga', 'CAR', 'NCR']

export default class NationalReportsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routeData: '/',
            redirect: false,
            demand: null,
            distribution: null,
            unmetReferred: null
        }
        this.ref = firebase.database().ref("data").child("regions");
    }

    componentDidMount() {
        const app = this;
    
        let regionBucket = [];
        let demandSet1 = [];
        let demandSet2 = [];
        let maleSet = [];
        let femaleSet = [];
        let coupleSet = [];
        let unmetSet = [];
        let referredSet = [];
        this.ref.on("value", s => {
            let data = s.val();
            if (data !== null) {
                async.forEachOf(data, function (value, key, callback) {
                    regionBucket.push(regions[key]);
                    demandSet1.push(value.demand.target);
                    demandSet2.push(
                        parseInt(value.demand.reached.couple) +
                        parseInt(value.demand.reached.male) +
                        parseInt(value.demand.reached.female)
                    );
                    unmetSet.push(value.demand.unmet);
                    referredSet.push(value.demand.referred);
                    maleSet.push(parseInt(value.demand.reached.male));
                    femaleSet.push(parseInt(value.demand.reached.female));
                    coupleSet.push(parseInt(value.demand.reached.couple));

                    callback();
                }, function (err) {
                    if (err) {
                        console.error(err.message);
                    } else {
                        app.setState({
                            demand: {
                                labels: regionBucket,
                                datasets: [
                                    {
                                        label: 'Targetted No. of Couples/ Individuals',
                                        backgroundColor: '#CDE0CD',
                                        borderColor: '#75A975',
                                        borderWidth: 1,
                                        data: demandSet1
                                    },
                                    {
                                        label: 'Total No. of Couples/ Individuals reached',
                                        backgroundColor: '#75A975',
                                        borderColor: '#75A975',
                                        borderWidth: 1,
                                        data: demandSet2
                                    }
                                ]
                            }, distribution: {
                                labels: [
                                    'Male',
                                    'Females',
                                    'Couples'
                                ],
                                datasets: [{
                                    data: [_.sum(maleSet), _.sum(femaleSet), _.sum(coupleSet)],
                                    backgroundColor: [
                                        '#569456',
                                        '#b8e7b8',
                                        '#75A975'
                                    ]
                                }]
                            }, unmetReferred: {
                                labels: regionBucket,
                                datasets: [
                                    {
                                        label: 'Couples/Individuals Reached',
                                        backgroundColor: '#eaf6ea',
                                        borderColor: '#75A975',
                                        borderWidth: 1,
                                        data: demandSet2
                                    },
                                    {
                                        label: 'Unmet Need Identified',
                                        backgroundColor: '#CDE0CD',
                                        borderColor: '#75A975',
                                        borderWidth: 1,
                                        data: unmetSet
                                    },
                                    {
                                        label: 'Referred and served',
                                        backgroundColor: '#75A975',
                                        borderColor: '#75A975',
                                        borderWidth: 1,
                                        data: referredSet
                                    }
                                ]
                            }
                        })
                    }
                });
            }
        })
    }

    printDoc = () => {
        let data = {
            demand_chart: this.demand_chart.chartInstance.canvas.toDataURL(),
            unmetReferred_chart: this.unmetReferred_chart.chartInstance.canvas.toDataURL(),
            distribution_chart: this.distribution_chart.chartInstance.canvas.toDataURL()
        }
        localStorage.setItem('charts', JSON.stringify(data));
        this.setState({routeData: null ,redirect:true})
    }
    render() {
        const { demand, distribution, unmetReferred } = this.state;
        if (this.state.redirect) {
            return <Redirect push to={`/dashboard/doc/print/${this.state.routeData}`} />;
        }
        return (
            <div>
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header>
                            <Header.Content>
                                <Breadcrumb>
                                    <Breadcrumb.Section as={Link} to="/dashboard/analytics">Generate Reports</Breadcrumb.Section>
                                    <Breadcrumb.Divider />
                                    <Breadcrumb.Section active>National Data</Breadcrumb.Section>
                                </Breadcrumb>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Button basic icon labelPosition='left' onClick={this.printDoc}>
                                <Icon name='print' />
                                Print
                            </Button>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container textAlign="left">
                    <Header content="Demand Generation" />
                    {demand !== null && <Bar
                        ref={chart => this.demand_chart = chart}
                        data={demand}
                        width={100}
                        height={30}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />}
                    <Header content="Couples/Individuals Reached vs Identified Unmet Need vs Referred/Served" />
                    {unmetReferred !== null && <Bar
                        ref={chart => this.unmetReferred_chart = chart}
                        data={unmetReferred}
                        width={100}
                        height={50}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />}  
                    <Header content="National Gender and Couples Distribution of Attendees" />
                    {distribution !== null && <Doughnut
                        ref={chart => this.distribution_chart = chart}
                        height={100}
                        options={{
                            maintainAspectRatio: true
                        }}
                        data={distribution} />}
                        <Header content="National Summary of Indicators" />
                        
                </Container>
            </div>
        )
    }
}