import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Form, Header, Image, Divider } from 'semantic-ui-react';
import '../App.css';
import ReactFireMixin from "reactfire";
import * as firebase from 'firebase';
import { setRegion } from '../actions/index';

//firebase.initializeApp(firebaseConfig);

class LoginForm extends Component {
    mixins: [ReactFireMixin]
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            loading: false,
            error: false,
            errorMessage: 'Sign in to start.',
            regionValue: '',
            regionError: false,
            regions: this.props.regionSelection
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleChange(e, { name, value }) {
        const state = { [name]: value }
        this.setState(state);
    }

    login = () => {
        const app = this;
        const data = app.state;
        firebase.auth().signInWithEmailAndPassword(data.email, data.password).then((user) => {
            var u = firebase.auth().currentUser;
            console.log(u);
            app.setState({ loading: false });
        }).catch(function (error) {
            // Handle Errors here.
            //var errorCode = error.code;
            app.setState({ loading: false });
            app.setState({ error: true, errorMessage: error.message });
            // ...
        });
    }

    handleLogin = () => {
        this.login()
    }

    render() {

        const { email, password, loading, error, errorMessage} = this.state;
        return (
            <Form className="login-form" loading={loading} onSubmit={this.handleLogin}>
                <Image src="assets/img/app-logo-full.png" />
                <Header as="h2" content="Sign In" />
                <Form.Field error={error}>
                    <label>Email</label>
                    <Form.Input placeholder='Email' name="email" type="email" value={email} onChange={this.handleChange} />
                </Form.Field>
                <Form.Field error={error}>
                    <label>Password</label>
                    <Form.Input placeholder='Password' name="password" type="password" value={password} onChange={this.handleChange} />
                </Form.Field>
                <p style={{ color: error ? '#9f3a38' : 'black' }}>{errorMessage}</p>
                <div className="text--right">

                    <Button as={Link} to="/">Cancel</Button>
                    <Button className="popcom-default" type="submit">Sign In</Button>

                </div>
                <Divider />
                <p className="text--center"><a href="/">Having trouble signing in?</a></p>
            </Form>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        region: state.region.value,
        regionSelection: state.region.selection,
        role: state.roles.activeRole
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onRegionChange: (value) => {
            dispatch(setRegion(value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);