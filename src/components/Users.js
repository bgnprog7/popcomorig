import React, { Component } from 'react';
import { Message, Progress, Menu, Header, Table, Container} from 'semantic-ui-react';
import * as firebase from 'firebase';
import AddUser from './AddUser';
const uuidv4 = require('uuid/v4');
export default class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            loading: true,
            percent: 0,
            regionData: []
        }
        this.ref = firebase.database().ref("users");
        this.regionsRef = firebase.database().ref("regions");
    }
//        {currentUser==='lrk70mX6qSXxYaivShHrpFBaHv22'?'Admin':'Client'}
    componentDidMount() {
        const app = this;
        const key = 'popcom_users';

        let offlineData = localStorage.getItem(key);

        if (offlineData !== null) {
            this.setState({ loading: false, users: JSON.parse(offlineData) })
        }

        // NOTE: Uses async operation
        this.regionsRef.once("value").then(r=>{
            app.setState({regionData:r.val()});
            app.ref.on("value", s => {
                localStorage.setItem(key, JSON.stringify(s.val()));
                app.setState({ users: s.val(), percent: 100, loading:false });
                console.log(r.val())
            });
        })
    }
    render() {
        const { users, percent, loading } = this.state;
        return (
            <div>
                <Progress color="green" autoSuccess active percent={percent} disabled={!loading} size='tiny' attached="top"/>      
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header textAlign="left">
                            <Header.Content>
                                Users
                            <Header.Subheader>
                                    Manage your users
                            </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <AddUser />
                    </Menu.Menu>
                </Menu>
                <Container textAlign="left">
                    <p className="text--left">User Management Module for POPCOM encoders.</p>
                    <Table singleLine>
                        <Table.Header>
                            <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Region ID</Table.HeaderCell>
                                <Table.HeaderCell>E-mail</Table.HeaderCell>
                                {/* <Table.HeaderCell>Actions</Table.HeaderCell> */}
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {Object.keys(users).map(user => {
                                return (
                                    <Table.Row key={uuidv4()}>
                                    <Table.Cell>{user}</Table.Cell>
                                        <Table.Cell>{users[user].displayName}</Table.Cell>
                                        <Table.Cell>{parseInt(users[user].regionID)+1}</Table.Cell>
                                        <Table.Cell>{users[user].email}</Table.Cell>
                                        {/* <Table.Cell>
                                            <Button
                                                basic
                                                icon
                                            >
                                                <Icon name='pencil' />
                                            </Button>
                                            <Button
                                                basic
                                                icon
                                            >
                                                <Icon name='delete' />
                                            </Button>
                                        </Table.Cell> */}
                                    </Table.Row>
                                )
                            })}
                        </Table.Body>
                    </Table>
                    <Message warning>
    <Message.Header>Alpha Version!</Message.Header>
    <p>Deleting users is currently disabled in this version. The default password for each user is: <b>1q2w3e4r5t6y</b></p>
  </Message>
                </Container>
            </div>
        )
    }
}

