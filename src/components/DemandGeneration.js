import React, { Component } from 'react';
import {Input, Form, Container, Card, Progress, Button, Menu, Header, Grid} from 'semantic-ui-react'
import * as firebase from 'firebase';
import moment from 'moment';
export default class DemandGeneration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reached: 0,
            referred: 0,
            target: 0,
            unmet: 0,
            male:0,
            female: 0,
            couple: 0,
            loading: true,
            region: {
                regCode: 0
            },
            remarks: '',
            percent: 0
        }
        this.regionDataRef = firebase.database().ref("data").child("regions");
        this.currentUser = firebase.auth().currentUser;
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e, { name, value }) {
        const state = { [name]: value }
        this.setState(state);
    }

    saveData = ()=>{
        this.setState({percent: 0, loading: true});
        const { target, male, female, couple, referred, unmet, region, remarks } = this.state;
        this.regionDataRef.child(parseInt(region.regCode) - 1).child("demand").update({
            reached: {
                male:male,
                female: female,
                couple:couple
            },
            referred: referred,
            target: target,
            unmet: unmet,
            remarks: remarks
        }).then(()=>{
            this.setState({percent: 100, loading:false});
        })
    }

    componentDidMount() {
        const key = 'demand';

        let offlineData = localStorage.getItem(key);

        if (offlineData !== null) {
            const data = JSON.parse(offlineData)
            this.setState({
                loading: false,
                male: data.male,
                female: data.female,
                couple: data.couple,
                referred: data.referred,
                target: data.target,
                unmet: data.unmet,
                remarks: data.remarks,
                percent: 10
             })
        }

        firebase.database().ref("users").child(this.currentUser.uid).child("regionID").once("value").then(s => {
            firebase.database().ref("regions").child(s.val()).once("value").then(r => {
                let region = r.val();
                this.setState({ region: region, percent: 50 });
                this.regionDataRef.child(parseInt(region.regCode) - 1).child("demand").on("value", d => {
                    let data = d.val();
                    if (data !== null) {
                        localStorage.setItem(key, JSON.stringify(d.val()))
                        this.setState({
                            loading: false,
                            male: data.reached.male,
                            female:data.reached.female,
                            couple: data.reached.couple,
                            referred: data.referred,
                            target: data.target,
                            unmet: data.unmet,
                            remarks: data.remarks,
                            percent: 100
                        })
                    }
                })
            })
        });
    }
    render() {
        const { target, loading, male, female, couple, referred, unmet, remarks, percent } = this.state;
        return (
            <div>
                <Progress color="green" autoSuccess active percent={percent} disabled={!loading} size='tiny' attached="top"/>      
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header textAlign="left">
                            <Header.Content>
                                Demand Generation
                                <Header.Subheader>
                                    Regional POPCOM Accomplishment Report
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Menu.Item>


                    <Menu.Menu position="right">
                        <Menu.Item>
                            <Header as="h1" color="green" textAlign="left" content={moment().format("MMMM Do YY")} />
                        </Menu.Item>
                    </Menu.Menu>

                </Menu>
                <Container textAlign="left">
                    <Card fluid>
                        <Card.Content>
                            <Form loading={loading}>
                                <Grid>
                                    <Grid.Row columns={2}>
                                        <Grid.Column>
                                            <Form.Field>
                                                <label>Targetted Number of Couples/Individuals</label>
                                                <Form.Input name="target" value={target} type="number" placeholder='0' onChange={this.handleChange} />
                                            </Form.Field>
                                            <Form.Field>
                                                <label>Total Number of Couples/Individuals Reached</label>
                                                                                               
                                                <Input style={{marginBottom: 5}} label={{ basic: true, content: 'Males' }} labelPosition='left' name="male" value={male} type="number" placeholder='0' onChange={this.handleChange} />                                                
                                                <Input style={{marginBottom: 5}} label={{ basic: true, content: 'Females' }} labelPosition='left'  name="female" value={female} type="number" placeholder='0' onChange={this.handleChange} />                                                
                                                <Input style={{marginBottom: 5}} label={{ basic: true, content: 'Couples' }} labelPosition='left'  name="couple" value={couple} type="number" placeholder='0' onChange={this.handleChange} />
                                                
                                            </Form.Field>
                                            <Form.Field>
                                                <label>Unmet Need Identified</label>
                                                <Form.Input name="unmet" value={unmet} type="number" placeholder='0' onChange={this.handleChange} />
                                            </Form.Field>
                                            <Form.Field>
                                                <label>Referred and Served</label>
                                                <Form.Input name="referred" value={referred} type="number" placeholder='0' onChange={this.handleChange} />
                                            </Form.Field>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <label>Remarks</label>
                                            <Form.TextArea name="remarks" value={remarks} placeholder="Enter your remarks here." onChange={this.handleChange}/>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>

                                <p style={{ textAlign: 'right' }}>
                                    <Button color="green" type='submit' onClick={this.saveData}>Save</Button>
                                </p>
                            </Form>
                        </Card.Content>
                    </Card>

                </Container>
            </div>
        )
    }
}