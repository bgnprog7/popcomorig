import React, { Component } from 'react';
import {Menu, Image, Header, Divider, Container} from 'semantic-ui-react'

export default class About extends Component {
    render() {
        return (
            <div>
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header textAlign="left">
                            <Header.Content>
                                About
<Header.Subheader>
                                    About POPCOM Data Management System
</Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position='right'>

                    </Menu.Menu>
                </Menu>
                <Container textAlign="left">
                    <Image src={require('../assets/img/popcom-doc-header.jpg')} size="medium" />
                    <Header as="h2" content="Commission on Population Data Management System" />
                    <p>
                        Commission on Population is one of the health agencies mandated to serve as the Central coordinating and policy making body of the government field of population. It was officially launched through the Executive Order No. 233.
</p>
                    <Divider />
                    <Header content="Vision" />
                    <p>
                        Responsible individuals, well-planned, prosperous healthy and happy families, empowered communities, guided by the Divine Providence living harmoniously and equitably in a sustainable environment.
</p>
                    <Header content="Mission" />
                    <p>
                        We are a technical and information resource agency, working in partnership with national and local government policy and decision makers, program implementers, community leaders and civil society.
</p>
                </Container>
            </div>
        )
    }
}