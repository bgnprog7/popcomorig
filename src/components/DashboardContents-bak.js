import React, { Component } from 'react';
import {
  Header,
  Divider,
  Tab,
  Container,
  Menu,
  Breadcrumb
} from 'semantic-ui-react';
import '../App.css';
import ReactFireMixin from "reactfire";
import ImpactIndicators from '../components/ImpactIndicators';
import Charts from '../components/Charts';
import ToggleDisplay from 'react-toggle-display';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import * as JsSearch from 'js-search';
import { setRegion } from '../actions/index';
// import { getStoredState } from 'redux-persist';

const uuidv1 = require('uuid/v1');

var search = new JsSearch.Search('key');

class DashboardContents extends Component {
  mixins: [ReactFireMixin]
  constructor(props) {
    super(props);
    this.state = {
      panes: [
        // { menuItem: 'Impact', render: () => <Tab.Pane><ImpactIndicators key={uuidv1()} path="impact" /></Tab.Pane> },
        // { menuItem: 'Outcome', render: () => <Tab.Pane><ImpactIndicators key={uuidv1()} path="outcome" /></Tab.Pane> },
        // { menuItem: 'Input', render: () => <Tab.Pane><ImpactIndicators key={uuidv1()} path="input" /></Tab.Pane> },
        // { menuItem: 'Output', render: () => <Tab.Pane><ImpactIndicators key={uuidv1()} path="output" /></Tab.Pane> },

        { menuItem: 'Impact', render: () => <Tab.Pane><ImpactIndicators key={uuidv1()} /></Tab.Pane> },
        { menuItem: 'Outcome', render: () => <Tab.Pane>{<p style={{margin:'10em auto'}}>No forms yet.</p>}</Tab.Pane> },
        { menuItem: 'Input', render: () => <Tab.Pane>{<p style={{margin:'10em auto'}}>No forms yet.</p>}</Tab.Pane> },
        { menuItem: 'Output', render: () => <Tab.Pane>{<p style={{margin:'10em auto'}}>No forms yet.</p>}</Tab.Pane> },
      ],
      show: true,
      activeItem: 'reports',
      region: "ncr"
    }
    const { regionSelection } = this.props;
    search.addIndex('key');
    regionSelection.map((value) => {
      search.addDocuments([value]);
      return null
    });
    this.ref = firebase.database().ref("user_settings");

  }
  toggleCharts = (e, { name }) => {
    this.setState({ show: !this.state.show, activeItem: name });
  }

  componentDidMount(){
    const user = firebase.auth().currentUser;
    // Set user region if null.
    if (user) {
      this.ref.child(user.uid).child("region").once("value").then(snapshot=>{
        const data = snapshot.val();
        if(data!==""){
          this.setState({region: data})
          this.props.onRegionChange(data);
        }else{
          this.ref.child(user.uid).set({region:this.props.region})
        }
      })      
    } else {
      console.log("User not signed in.")
    }
  }

  render() {
    const { panes, activeItem } = this.state;
    const { region } = this.state;
    const regionObject = search.search(region);
    const MainMenu = (
      <Menu attached='top'>
        <Menu.Item as="a" name='REPORTS'>
          <Breadcrumb size='small'>
            <Breadcrumb.Section>{regionObject.length === 1 ? regionObject[0].text : 'Region'}</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section>POPDEV</Breadcrumb.Section>
          </Breadcrumb>
        </Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item name="reports" as="a" content="Reports" icon="file text outline" onClick={this.toggleCharts} active={activeItem === 'reports'} />
          <Menu.Item name="visualizations" as="a" content="Visualizations" icon="pie chart" onClick={this.toggleCharts} active={activeItem === 'visualizations'} />
        </Menu.Menu>
      </Menu>
    )

    return (
      <div className="dashboard-contents">
        <ToggleDisplay show={this.state.show}>
          <Container>
            {MainMenu}
            <br />
            <Tab panes={panes} />
          </Container>
        </ToggleDisplay>
        <ToggleDisplay show={!this.state.show}>
          <Container>
            
            {MainMenu}
            <Divider />
            <Charts region={this.state.region} />
          </Container>
        </ToggleDisplay>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {

  return {
    region: state.region.value,
    regionSelection: state.region.selection
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onRegionChange: (value) => {
      dispatch(setRegion(value))
  }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContents);