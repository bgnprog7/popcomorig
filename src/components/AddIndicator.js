import React, { Component } from 'react';
import {Dropdown, Button, Header, Modal, Input, Icon } from 'semantic-ui-react'
import * as firebase from 'firebase';

//firebase.initializeApp(firebaseConfig);

const sourceOptions = [
    { key: 'rpfp', value: 'rpfp', text: 'RPFP' },
    { key: 'popdev', value: 'popdev', text: 'POPDEV' },
    { key: 'ahyd', value: 'ahyd', text: 'AHYD' }
]

const domainOptions = [
    { key: 'impact', value: 'impact', text: 'Impact' },
    { key: 'output', value: 'output', text: 'Output' },
    { key: 'outcome', value: 'outcome', text: 'Outcome' },
    { key: 'input', value: 'input', text: 'Input' }
]

export default class EditIndicator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: '',
            domain: '',
            attendees: 0,
            remarks: "Loading...",
            validation: 0,
            loading:false,
            value: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.ref = firebase.database().ref("indicators").child("template");
    }
    handleChange(e, { name, value }) {
        const state = { [name]: value }
        this.setState(state);
    }
    state = { modalOpen: false }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    handleSave = () => {
        const app = this;
        const {source, domain, attendees, remarks, validation, value} = this.state;
        if(this.modal){
            this.setState({loading:true})
        }
        this.ref.child(source+"/"+domain+"/").once("value").then(s=>{
            
            app.ref.child(source+"/"+domain+"/").push({
                source: 0,
                attendees: 0,
                remarks: "none",
                validation: 0,
                value:0,
                name: value,
                timestamp: firebase.database.ServerValue.TIMESTAMP
            }).then(()=>{
                if(app.modal){
                    app.setState({loading:false, modalOpen:false})
                }
            })
        })
       
    }

    componentDidMount(){
        this.ref.once("value").then(s=>{
            if(s.val()!==null){
                if(this.modal){
                    this.setState(s.val())
                }
            }
        })
    }

    render() {
        const {source, attendees, domain, remarks, validation, value} = this.state;
        return (
            <Modal
                ref={(modal) => { this.modal = modal; }}
                trigger={<Button style={{margin:10}} fluid onClick={this.handleOpen} color="green">
                <Icon name="add" />
                Add Indicator
                </Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='mini'
            >
                <Header icon='pencil' content={"Add Indicator"} />
                <Modal.Content>
                    <p>Indicator Source</p>
                    <Dropdown name="source" placeholder='Select Source' value={source} onChange={this.handleChange} search selection options={sourceOptions} />
                    <br /><br />
                    <p>Indicator Domain</p>
                    <Dropdown name="domain" placeholder='Select Domain' value={domain} onChange={this.handleChange} search selection options={domainOptions} />
                    <br /><br />
                    <p>Indicator Name</p>
                    <Input name="value" placeholder='Indicator Name' value={value} onChange={this.handleChange}/>
                </Modal.Content>
                <Modal.Actions>
                <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button color='green' onClick={this.handleSave}>
                        Add
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}