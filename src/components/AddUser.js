import React, { Component } from 'react';
import { Dropdown, Button, Icon, Header, Modal, Input } from 'semantic-ui-react'
import * as firebase from 'firebase';
import async from 'async';

//firebase.initializeApp(firebaseConfig);


export default class AddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayName: '',
            email: '',
            regionID: 0,
            password: '',
            loading: false,
            regionData: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.ref = firebase.database().ref("users");
        this.regionsRef = firebase.database().ref("regions");
    }

    handleChange(e, { name, value }) {
        const state = { [name]: value }
        this.setState(state);
    }

    state = { modalOpen: false }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    handleSave = () => {
        const { displayName, email, password, regionID } = this.state;
        if (this.modal) {
            this.setState({ loading: true })
        }

        firebase.auth().createUserWithEmailAndPassword(email, password).then((user) => {
            return this.ref.child(user.uid).update({
                displayName: displayName,
                email: email,
                regionID: regionID,
                timestamp: firebase.database.ServerValue.TIMESTAMP
            })
        }).then(() => {
            if (this.modal) {
                this.setState({ loading: false, modalOpen:false })
            }
        }).catch(function (error) {
            // Handle Errors here.
            var errorMessage = error.message;
            console.log(errorMessage)
        });
    }

    componentDidMount() {
        const app = this;
        let regionData = [];
        // NOTE: Uses async operation
        this.regionsRef.once("value").then(s=>{
            async.forEachOf(s.val(), function (value, key, callback) {
                // NOTE: Replicate this to other marker methods
                  regionData.push(
                    { key: key, value: key, text: value.regDesc }
                  )
                  callback();
              }, function (err) {
                  if (err) {
                      console.error(err.message);
                  } else {
                      app.setState({regionData:regionData})
                  }
              });
        })
    }

    render() {
        const { displayName, email, password, regionID, regionData } = this.state;
        return (
            <Modal
                ref={(modal) => { this.modal = modal; }}
                trigger={<Button style={{ margin: 10 }} onClick={this.handleOpen} color="green"><Icon name='add' />
                    Add User</Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='mini'
            >
                <Header icon='pencil' content="Profile Details" />
                <Modal.Content>
                    
                    <p>Name</p>
                    <Input name="displayName" value={displayName} placeholder='User Name' onChange={this.handleChange} />
                    <br /><br />
                    <p>Email</p>
                    <Input name="email" type="email" value={email} placeholder='Email' onChange={this.handleChange} />
                    <br /><br />
                    <p>Password</p>
                    <Input type="password" name="password" placeholder='Password' value={password} onChange={this.handleChange} />
                    <br /><br />
                    <p>Region</p>
                    <Dropdown name="regionID" placeholder='Select Region' value={regionID} search selection options={regionData} onChange={this.handleChange} />

                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.handleClose}>
                        Cancel
                    </Button>
                    <Button color='green' onClick={this.handleSave}>
                        Save
                    </Button>
                </Modal.Actions>

            </Modal>
        )
    }
}