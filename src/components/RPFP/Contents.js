import React, { Component } from 'react';
import {Container, Progress, Menu, Header, Tab} from 'semantic-ui-react'
import * as firebase from 'firebase';
import OutcomeIndicators from './OutcomeIndicators';
import ImpactIndicators from './ImpactIndicators';
import InputIndicators from './InputIndicators';
import OutputIndicators from './OutputIndicators';
import moment from 'moment';
const panes = [
    { menuItem: 'Impact', render: () => <Tab.Pane><ImpactIndicators /></Tab.Pane> },
    { menuItem: 'Outcome', render: () => <Tab.Pane><OutcomeIndicators/></Tab.Pane> },    
    { menuItem: 'Output', render: () => <Tab.Pane><OutputIndicators/></Tab.Pane> },  
    { menuItem: 'Input', render: () => <Tab.Pane><InputIndicators/></Tab.Pane> }, 
]

export default class Contents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            percent: 0,
            region: {
                id: 0,
                regCode: 0,
                regDesc: 'Loading region...'
            }
        }
        this.currentUser = firebase.auth().currentUser
    }

    componentDidMount() {
        let key = "active_region";
        let offlineRegionData = localStorage.getItem(key);

        if (offlineRegionData !== null) {
            this.setState({ loading: false, region: JSON.parse(offlineRegionData), percent: 3 })
        }
        this.setState({loading:true});
        firebase.database().ref("users").child(this.currentUser.uid).child("regionID").once("value").then(s => {
            this.setState({ percent: 30 })
            firebase.database().ref("regions").child(s.val()).once("value").then(r => {
                localStorage.setItem(key, JSON.stringify(r.val()))
                this.setState({ region: r.val(), loading:false, percent: 100 })
            });
            
            firebase.database().ref("data").child("regions").child(s.val()).once("value").then(d=>{
                if(d.val()===null){
                    firebase.database().ref("indicators/template").once("value").then(i=>{
                        firebase.database().ref("data").child("regions").child(s.val()).update(i.val())
                    })
                }
            })

        })
    }

    render() {
        const { region, loading, percent } = this.state
        return (
            <div>          
                <Progress color="green" autoSuccess active percent={percent} disabled={!loading} size='tiny' attached="top"/>      
                <Menu pointing secondary>
                
                    <Menu.Item>
                        <Header textAlign="left">
                            <Header.Content>
                                RPFP - {region.regDesc}
                                <Header.Subheader>
                                Responsible Parenthood-Family Planning
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position="right">
                    <Menu.Item>
                    <Header as="h1" color="green" textAlign="left" content={moment().format("MMMM Do YYYY")} />
                    </Menu.Item>
                    </Menu.Menu>
                </Menu>
                
                <Container>
                    <Tab panes={panes} />
                </Container>
            </div>
        )
    }
}