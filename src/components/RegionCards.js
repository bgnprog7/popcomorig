import React, {Component} from 'react';
import { Label, Form, Message, Button, Card, Container } from 'semantic-ui-react'
import * as firebase from 'firebase';
import {
    Link
  } from 'react-router-dom';
import _ from 'lodash';
const uuidv4 = require('uuid/v4');

export default class RegionCards extends Component {
    constructor(props){
        super(props);
        this.state = {
            national_data: null,
            data_length: 0,
            loading: true
        }
        this.ref = firebase.database().ref("data/regions");
    }

    componentDidMount(){
        const app = this;
        const key = 'national_data';

        let offlineData = localStorage.getItem(key);

        if (offlineData !== null) {
            this.setState({ national_data: JSON.parse(offlineData) })
        }

        this.ref.once("value").then(s=>{
            let data = s.val()
            localStorage.setItem(key, JSON.stringify(data));
            app.setState({national_data:data, data_length: Object.keys(data).length, loading:false});
        })
    }

    setActiveRegion = (value)=>{
        console.log(value)
        const key = 'active_region';
        localStorage.setItem(key, value);
    }
    
    render(){
        const {data_length, loading} = this.state;
        return(
            <Container textAlign="left">
                <Message positive>
    <Message.Header>Dynamic Data</Message.Header>
    <p>System will not display non-existent region data.</p>
  </Message>
  <Message warning>
  <Message.Header>Disable Adblock</Message.Header>
    <p>Enabling Adblock will prevent the system to generate PDF Data.</p>
  </Message>
            <Card.Group>
            <Card color="green">
              <Card.Content textAlign="left">
              <Card.Header content="National Data"/>
              <Label color='red' circular floating size="big">{data_length}</Label>
              </Card.Content>
              <Card.Content textAlign="left" description="Generates report based on National Data. You can open the test data for referrence." />
              <Card.Content extra textAlign="right">
              <Button color="orange" basic content="Test Data" as={Link} to="/dashboard/dummy_data" />
                <Button color="green" basic circular icon='search' as={Link} to="/dashboard/national_data" />
              </Card.Content>
            </Card>
            {loading&&
             <Card>                
             <Card.Content header={"Loading regions..."} textAlign="left" />
             <Card.Content>
             <Form loading={loading}>  
             </Form>  
            </Card.Content>                         
             <Card.Content extra textAlign="right">
               <Button disabled basic circular icon='search' as={Link} to="/dashboard/regional_data" />
             </Card.Content>             
           </Card>                                 
            }
            {_.range(0, data_length).map(i =>
                <Card key={uuidv4()}>
                  <Card.Content header={"Region " + (i+1) + " Data"} textAlign="left" />
                  <Card.Content textAlign="left" description="Generates report based on Regional Data" />
                  <Card.Content extra textAlign="right">
                    <Button basic circular icon='search' as={Link} to={`/dashboard/regional_data/${ i }`} />
                  </Card.Content>
                </Card>              
            )}
          </Card.Group>
          </Container>
        )
    }
}

