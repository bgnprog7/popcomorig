import React, { Component } from 'react';
import {
    Header,
    Divider,
    Menu,
    Container,
    Breadcrumb
} from 'semantic-ui-react';
import * as firebase from 'firebase';
import { Doughnut, Bar } from 'react-chartjs-2';

import {
    Link,
    Redirect
} from 'react-router-dom';

let regions = ['I', 'II', 'III', 'IV-A', 'IV-B', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'ARMM', 'Caraga', 'CAR', 'NCR']

export default class ReportsPreview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routeData: '/',
            redirect: false,
            data1:{
                labels: regions,
                datasets: [
                    {
                        label: 'Targetted No. of Couples/ Individuals',
                        backgroundColor: '#CDE0CD',
                        borderColor: '#75A975',
                        borderWidth: 1,
                        data: [45168, 31231, 41556, 60656, 35000, 84572, 77883, 62876, 74608, 61586, 68093, 53458, 43143, 70811, 42057, 19133, 75811]
                    },
                    {
                        label: 'Total No. of Couples/ Individuals reached',
                        backgroundColor: '#75A975',
                        borderColor: '#75A975',
                        borderWidth: 1,
                        data: [16178, 7768, 13441, 18579, 21637, 35456, 25536, 29486, 30008, 16182, 12436, 150490, 22454, 67950, 22992, 12728, 30896]
                    }
                ]
            },
            data2:{
                labels: regions,
                datasets: [
                    {
                        label: 'Couples/Individuals Reached',
                        backgroundColor: '#eaf6ea',
                        borderColor: '#75A975',
                        borderWidth: 1,
                        data: [16178, 7768, 13441, 18579, 21637, 35456, 25536, 29486, 30008, 16182, 12436, 150490, 22454, 67950, 22992, 12728, 30896]
                    },
                    {
                        label: 'Unmet Need Identified',
                        backgroundColor: '#CDE0CD',
                        borderColor: '#75A975',
                        borderWidth: 1,
                        data: [1930, 862, 1573, 4938, 4366, 5771, 7708, 11430, 10605, 2111, 3219, 1268, 4444, 11868, 3316, 3101, 14945]
                    },
                    {
                        label: 'Referred and served',
                        backgroundColor: '#75A975',
                        borderColor: '#75A975',
                        borderWidth: 1,
                        data: [2043, 2379, 617, 1999, 2262, 5226, 5607, 3307, 4951, 1070, 2214, 7519, 3486, 9859, 967, 665, 14015]
                    }
                ]
            },
            data3:{
                labels: [
                    'Non 4Ps',
                    '4Ps',
                    'PMC',
                    'USAPAN'
                ],
                datasets: [{
                    data: [47, 34, 14, 5],
                    backgroundColor: [
                        '#569456',
                        '#b8e7b8',
                        '#75A975',
                        '#b8e7b8',
                    ]
                }]
            }

        }
        this.ref = firebase.database().ref("testdatasets");

    }

    componentDidMount() {
        const app = this;
        
        this.ref.on("value", s=>{
            let data = s.val();
            if(data!==null){
                app.setState({data1: {
                    labels: regions,
                    datasets: [
                        {
                            label: 'Targetted No. of Couples/ Individuals',
                            backgroundColor: '#CDE0CD',
                            borderColor: '#75A975',
                            borderWidth: 1,
                            data: data.data1
                        },
                        {
                            label: 'Total No. of Couples/ Individuals reached',
                            backgroundColor: '#75A975',
                            borderColor: '#75A975',
                            borderWidth: 1,
                            data: data.data2
                        }
                    ]
                }})
            }
        })
    }

    printDoc = () => {
        let data = {
            chart1: this.chart1.chartInstance.canvas.toDataURL(),
            chart2: this.chart2.chartInstance.canvas.toDataURL(),
            chart3: this.chart3.chartInstance.canvas.toDataURL()
        }
        localStorage.setItem('charts', JSON.stringify(data));
        this.setState({routeData: null ,redirect:true})

        // console.log(data);
        // this.setState({routeData:`/dashboard/doc/print/${data}`})

        // this.props.history.push(`/dashboard/doc/print/${{data:{
        //     chart1: this.chart1.chartInstance.canvas.toDataURL(),
        //     chart2: this.chart2.chartInstance.canvas.toDataURL(),
        //     chart3: this.chart3.chartInstance.canvas.toDataURL()
        // }}}`)
    }
    render() {
        if (this.state.redirect) {
            return <Redirect push to={`/dashboard/doc/print/${this.state.routeData}`} />;
        }
        return (
            <div>
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header>
                            <Header.Content>
                                <Breadcrumb>
                                    <Breadcrumb.Section as={Link} to="/dashboard/analytics">Generate Reports</Breadcrumb.Section>
                                    <Breadcrumb.Divider />
                                    <Breadcrumb.Section active>National Data</Breadcrumb.Section>
                                </Breadcrumb>
                            </Header.Content>
                        </Header>
                    </Menu.Item>
                </Menu>
                <Container textAlign="left">
                    <Header content="Demand Generation" />
                    <Bar
                        ref={chart => this.chart1 = chart}
                        data={this.state.data1}
                        width={100}
                        height={30}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />
                    <br />
                    <p>The first semester accomplishment for RPFP classes is 534,252 couples/individuals or 56.40% of the total 947,077 target for 2017. In terms of individuals/couples reached, POPCOM XI reached the highest of 150,490 couples/individuals accomplishment, followed by POPCOM ARMM of 67,950. It is noted however that for the month of June alone POPCOM XI reached a total of 106,087 couples/individual for the 10, 254 classes conducted, as reported in the Form A.</p>
                    <Divider />
                    <Header content="Couples/Individuals Reached vs Identified Unmet Need vs Referred/Served" />
                    <Bar
                        ref={chart => this.chart2 = chart}
                        data={this.state.data2}
                        width={100}
                        height={50}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />
                    <Header content="Distribution of 534,252 Couples/Individuals of Reproductive Age reached per Class Type" />
                    <div style={{ width: 'auto', height: '400px', margin: '0 auto' }}>
                        <Doughnut
                            ref={chart => this.chart3 = chart}
                            height={100}
                            options={{
                                maintainAspectRatio: true
                            }}
                            data={this.state.data3} />
                    </div>
                    <p>Of the 534,252 couples/individuals reached , Non-4Ps ranks 1 st (47%), 4Ps ranks 2nd with 34% followed by PMC of 14% and Usapan of 5%.</p>
                </Container>
            </div>
        )
    }
}