import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Breadcrumb, Menu, Icon, Header} from 'semantic-ui-react'
import '../App.css';
import _ from 'lodash';
const pdfMake = require('pdfmake/build/pdfmake.js');
const pdfFonts = require('pdfmake/build/vfs_fonts.js');
const POPCOMLogo = require('../assets/base64/POPCOMLogo');

//firebase.initializeApp(firebaseConfig);
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default class PDF extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.documentData
        };

    }
    getChartData = ()=> {
        console.log(this.chart.chartInstance.canvas.toDataURL())
    }


    componentDidMount(){
        let d = new Date();
        
        var chartData = JSON.parse(localStorage.getItem('charts'));
        let docDefinition = { 
            content: [
                {
                    image: POPCOMLogo,
                    fit: [180, 180],
                },
                {
                    text: 'Report 2018',
                    color: '#235323',
                    fontSize: 20,
                    marginTop: 20
                },
                {
                    text: 'Generated on '+d,
                    color: '#333',
                    fontSize: 7,
                },
                {
                    text: 'Message from the Executive Director',
                    color: '#235323',
                    fontSize: 8,
                    marginTop: 20
                },
                {
                    text: 'It has been a year of changes and adjustments for the Commission on Population and the Philippine Population Program (PPP) that it coordinates and manages.',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },
                {
                    text: 'The PPP has been on the priority list of the President, Gloria Macapagal Arroyo, and her Cabinet in 2006. POPCOM was directed to make the Program highlight the practice of natural family planning (NFP). Following this directive, the mid-year Philippine Population Directional Plan and Strategy was modified and delivered to Malacanang and approved in May to pave the way for a Responsible Parenthood-Natural Family Planning (RP-NFP) Program',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },  
                {
                    text: 'As central and regional levels, POPCOM formulated and carried out an aggressive and systematic strategy to advocate and promote the new Program by planning activities to deliver the president’s opted themes of responsible parenting, NFP, birth spacing and breastfeeding in its materials development and production activities.',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },
                {
                    text: 'Towards garnering support and participation for the modified program among the widest sectors at all levels, POPCOM continued to strengthen and widen its alliances with other National Government Agencies (NGAs), Non-Government Organizations (NGOs), local government units (LGUs), business and private sectors, academe, and faith-based institutions.',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                }, 
                {
                    text: 'As the government’s coordinating arm for population concerns, POPCOM continued to hold dialogues and explore possible areas of collaboration with the Catholic Church in implementing the President’s mandate on the population program.',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                }, 
                {
                    text: 'But this was no reason for POPCOM to neglect its other program commitments, which it considers equally significant in moving the population program goals forward. Additional POPDEV sectoral frameworks for Population and Energy, Population and Physical Infrastructure and Population & Peace and Security were finally validated with the respective agencies which deal with such sectors like Department of Environment and Natural Resources (DENR), Department of Interior and Local Government (DILG), Local Government Unites (LGUs), etc. Also, a popularized version of the Gender- Responsive POPDEV Planning Guide was developed and produced to promote and widen the efficient use of the POPDEV planning approach in local development planning. Empowering the youth by deepening their awareness and understanding of population and development issues that affect their lives were the ultimate aims of such activities as a nationwide Essay Writing and the 2006 Population Quiz Show.',
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                }, 
                {
                    text: "In the face of challenges, POPCOM, like an orchestra conductor, gets the acts of the program together- whether old, new or continuing- by assembling, arranging, fine-tuning, moderating or in program terms, coordinating what its population partners, whether government or private, singly or together, can contribute to the Philippine Population Management Program (PPMP) in the pusuit of its overall objecttives. All in a year's work.",
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                }, 
                {
                    text: '-Tomas M. Osias',
                    fontSize: 10,
                    color: '#235323',
                    marginTop: 20
                },                     
                {
                    image: POPCOMLogo,
                    fit: [180, 180],
                    marginTop: 300
                },
                {
                    text: 'IEC, Advocacy Materials and Mass Media Release',
                    color: '#235323',
                    fontSize: 13,
                    marginTop: 20
                },
                {
                    text: "Press releases and advertisements on population issues and concerns were aired in local radio stations and published in local newspapers. IEC/ Advocacy materials such as media guide, SPPR info kits, bookmarks, RP Handbook, streamers,leaflets, ARH Info sheet, and newsletters were developed/produced and disseminated at the national, regional and local levels to streng then the advocacy efforts in the regions. A total of 35 types were developed and 63,818 copies were produced.",
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },
                {
                    text: 'Demand Generation',
                    color: '#235323',
                    fontSize: 13,
                    marginTop: 20
                },
                {
                    image: chartData.demand_chart,
                    fit: [500, 500],
                },
                {
                    text: "The first semester accomplishment for RPFP classes is 534,252 couples/individuals or 56.40% of the total 947,077 target for 2017. In terms of individuals/couples reached, POPCOM XI reached the highest of 150,490 couples/individuals accomplishment, followed by POPCOM ARMM of 67,950. It is noted however that for the month of June alone POPCOM XI reached a total of 106,087 couples/individual for the 10, 254 classes conducted, as reported in the Form A.",
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },
                {
                    text: 'Couples/Individuals Reached vs Identified Unmet Need vs Referred/Served',
                    color: '#235323',
                    fontSize: 13,
                    marginTop: 20
                },
                {
                    image: chartData.unmetReferred_chart,
                    fit: [500, 500],
                },
                {
                    text: "",
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },
                {
                    text: 'Distribution of 534,252 Couples/Individuals of Reproductive Age reached per Class Type',
                    color: '#235323',
                    fontSize: 13,
                    marginTop: 50
                },
                {
                    image: chartData.distribution_chart,
                    fit: [500, 500],
                },
                {
                    text: "Of the 534,252 couples/individuals reached , Non-4Ps ranks 1 st (47%), 4Ps ranks 2nd with 34% followed by PMC of 14% and Usapan of 5%.",
                    fontSize: 10,
                    color: '#333',
                    marginTop: 5
                },

                     
            ]
        }
        switch(this.props.action){
            case "download":
            pdfMake.createPdf(docDefinition).download("popcom_report.pdf");            
            break;
            case "open":
            pdfMake.createPdf(docDefinition).open();
            break;
            case "print":
            pdfMake.createPdf(docDefinition).print();
            break;
        }   
    }

    render() {
        return (
            <div>
            <Menu pointing secondary>
            <Menu.Item>
              <Header textAlign="left">
                <Header.Content>
                <Breadcrumb>
                <Breadcrumb.Section textAlign="left" as={Link} to="/dashboard/analytics">Generate Reports</Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section active>{_.startCase(this.props.action)}</Breadcrumb.Section>
              </Breadcrumb>
                </Header.Content>
              </Header>
            </Menu.Item>
            </Menu>

            <div style={{padding: '20%'}}>
                <Header content={"Finished "+this.props.action+"ing report."} />
                {/* <Doughnut ref={chart=>this.chart=chart} data={data} /> */}
                {/* <Button content="Get Data" onClick={this.getChartData} /> */}
                <Icon size="huge" name="print"/>
            </div>
            </div>
        )
    }
}