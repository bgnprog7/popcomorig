import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Form,
  Header,
  Image,
  Divider,
  Message,
  Tab,
  Container,
  Icon,
  Label,
  Menu,
  Table
} from 'semantic-ui-react';
import _ from 'lodash';
import ReactFireMixin from "reactfire";
import * as firebase from 'firebase';
import CollapsibleDropdown from '../components/CollapsibleDropdown';
import CollapsibleTextInput from '../components/CollapsibleTextInput';
import '../App.css';

const uuidv1 = require('uuid/v1');
  
  const MenuOptions = [
    { key: 'edit', icon: 'edit', text: 'Edit Post', value: 'edit' },
    { key: 'delete', icon: 'delete', text: 'Remove Post', value: 'delete' },
    { key: 'hide', icon: 'hide', text: 'Hide Post', value: 'hide' },
  ]

export default class LoginForm extends Component {
    mixins: [ReactFireMixin]
    constructor(props){
        super(props);
        this.state = {
            rows: [],
            loading: true,
            indicatorValue: null,
            value: [],
            indicators: [
              { key: 'a', value: 'population_size', text: 'Population Size' },
              { key: 'b', value: 'number_of_households', text: 'Number of Households' },
              { key: 'c', value: 'population_growth_rate', text: 'Population Growth Rate (PGR)' },
              { key: 'd', value: 'population_by_sex_and_sex_ratio', text: 'Population by Sex and Sex Ratio' },
              { key: 'e', value: 'population_by_age_group', text: 'Population by Age Group' },
              { key: 'f', value: 'population_density', text: 'Population Density' },
              { key: 'g', value: 'total_age_dependency_ratio', text: 'Total Age Dependency Ratio' },
              { key: 'h', value: '8', text: 'Young Age Dependency Ratio' },
              { key: 'i', value: '9', text: 'Old Age Dependency Ratio' },
              { key: 'j', value: '10', text: 'Crude Birth Rate' },
              { key: 'k', value: '11', text: 'General Fertility Rate' },
              { key: 'l', value: '12', text: 'Age Specific Fertility Rate' },
              { key: 'm', value: '13', text: 'Total Fertility Rate' },
              { key: 'n', value: '14', text: 'Number of In-Migrants' },
              { key: 'o', value: '15', text: 'Number of Out-Migrants' },
              { key: 'p', value: '16', text: 'Number of Livebirths' },
              { key: 'q', value: '17', text: 'Crude Death Rate' },
              { key: 'r', value: '18', text: 'Percentage of underweight children' },
              { key: 's', value: '19', text: 'Leading Causes of Mortality' },
              { key: 't', value: '20', text: 'Life Expectancy at Birth' },
              { key: 'u', value: '21', text: 'Infant Mortality Rate' },
              { key: 'v', value: '22', text: 'Maternal Mortality Rate' },
              { key: 'w', value: '23', text: 'Hospital Bed to Population Ratio' },
              { key: 'x', value: '24', text: 'Doctor to Population Ratio' },
              { key: 'y', value: '25', text: 'Number of Health Facilities' },
              { key: 'z', value: '26', text: 'Percentage of Population by Highest Educational Attainment' },
              { key: 'aa', value: '27', text: 'Elementary Participation Rate' },
              { key: 'ab', value: '28', text: 'Secondary Participation Rate' },
              { key: 'ac', value: '29', text: 'Drop Out Rate' },
              { key: 'ad', value: '30', text: 'Simple Literacy Rate' },
              { key: 'ae', value: '31', text: 'Number of Primary Schools' },
              { key: 'af', value: '32', text: 'Student to Teacher Ratio' },
              { key: 'ag', value: '33', text: 'Student to Classroom Ratio' },
              { key: 'ah', value: '34', text: 'Labor Force Participation Rate' },
              { key: 'ai', value: '35', text: 'Employment Rate' },
              { key: 'aj', value: '36', text: 'Unemployment Rate' },
              { key: 'ak', value: '37', text: 'Under-employment Rate' },
              { key: 'al', value: '38', text: 'Average Annual Consumption of Rice' },
              { key: 'am', value: '39', text: 'Total Volume of Palay Produced' },
              { key: 'an', value: '40', text: 'Quantity of Fish Production (metric tons)' },
              { key: 'ao', value: '41', text: 'Status of Land Classification (in hectares)' },
              { key: 'ap', value: '42', text: 'Number of Households with Access to Sanitary Toilets' },
              { key: 'aq', value: '43', text: 'Gross Regional Domestic Product' },
              { key: 'ar', value: '44', text: 'Gross Regional Domestic Product' },
              { key: 'as', value: '45', text: 'Per Capital Poverty Threshold' },
              { key: 'at', value: '46', text: 'Poverty Incidence among Families' },
              { key: 'au', value: '47', text: 'Poverty Incidence among Population' },
              { key: 'av', value: '48', text: 'Number of Households enlisted in the NHTS' },
              { key: 'aw', value: '49', text: 'Number of Households enlisted as Pantawid Beneficiary' },
            ],
            geographicAreas: [
              { key: 'a', value: 'municipality_1', text: 'Municipality 1' },
              { key: 'b', value: 'municipality_2', text: 'Municipality 2' },
              { key: 'c', value: 'municipality_3', text: 'Municipality 3' },
              { key: 'd', value: 'municipality_4', text: 'Municipality 4' },
              { key: 'e', value: 'municipality_5', text: 'Municipality 5' },
            ],
            selected: ['population_size']
        }
        this.addRow = this.addRow.bind(this);
        this.removeRow = this.removeRow.bind(this)
        this.ref = firebase.database().ref("indicators/"+this.props.path);
    }

    componentDidMount(){        
        const {rows} = this.state;
        this.ref.on("value", (snap)=>{
            if(snap.val()!==null){
                this.setState({rows:snap.val()});
            }
            this.setState({loading:false})           
        })
    }

    componentWillUnmount(){
        this.ref.off()
    }

    addRow(){
        const {rows} = this.state;
        this.ref.push({indicator:'Select Indicator'});        
    }

    removeRow(key){
        console.log(this.state.rows)

        const {rows} = this.state;        

        delete rows[key];

        this.ref.child(key).remove();

        this.setState({rows:rows});

        
    }

    handleChange = (e, { value }) => {
      this.setState({value});
    };

    render(){
        const {rows, loading, indicators, selected, value, geographicAreas} = this.state;
        return(
            <Form loading={loading}>
            <Table celled striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Indicators</Table.HeaderCell>
                <Table.HeaderCell>Geographic Area</Table.HeaderCell>
                <Table.HeaderCell>Period</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
        
            <Table.Body>
              {rows!==[]&&Object.keys(rows).map((item)=>(
                  <Table.Row key={uuidv1()}>
                  <Table.Cell>
                    <CollapsibleDropdown
                      fluid search selection
                      options={indicators}
                      selection={_.startCase(rows[item].indicator)}
                      placeholder='Select Indicator'
                      firebaseRef={this.ref.child(item)}
                      childKey='indicator'
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <CollapsibleDropdown
                      fluid search selection
                      options={geographicAreas}
                      selection={_.startCase(rows[item].area)}
                      placeholder='Select Area'
                      firebaseRef={this.ref.child(item)}
                      childKey="area"
                    />
                  </Table.Cell>
                  <Table.Cell >
                    <CollapsibleTextInput fluid 
                    placeholder='Year'
                    firebaseRef={this.ref.child(item)}
                    childKey="year"
                    text={rows[item].year}            
                    />
                  </Table.Cell>
                  <Table.Cell collapsing textAlign="center">
                    <Button basic icon onClick={()=>this.removeRow(item)}>
                      <Icon name='trash' />
                    </Button>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
        
            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='4'>

                  <Menu floated='right'> 
                    <Menu.Item as="a" content="Add Entry" icon="add" onClick={()=>this.addRow()} />                    
                  </Menu>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
          </Form>
        )
    }
}