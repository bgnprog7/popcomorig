import React, { Component } from 'react';
import {Form, Table} from 'semantic-ui-react'
import EditIndicator from '../EditIndicator';
import * as firebase from 'firebase';
const uuidv4 = require('uuid/v4');
const sourceOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'psa', value: 1, text: 'PSA' },
    { key: 'popcom', value: 2, text: 'POPCOM' },
    { key: 'doh', value: 3, text: 'DOH' },
    { key: 'dswd', value: 4, text: 'DSWD' },
]

const attendeesOptions = [
    { key: 'none', value: 0, text: 'Not Set' },
    { key: 'male', value: 1, text: 'Male' },
    { key: 'female', value: 2, text: 'Female' },
    { key: 'couples', value: 3, text: 'Couples' }
]

const indicatorPath = "outcome";

export default class POPDEVInputIndicators extends Component {
    constructor(props) {
        super(props);
        this.regionDataRef = firebase.database().ref("data").child("regions");
        this.currentUser = firebase.auth().currentUser;
        this.state = {
            region: {
                regCode: 0
            },
            data: [],
            loading: true
        }
    }

    componentDidMount() {
        const key = 'popdev_'+indicatorPath+'_indicators';

        let offlineData = localStorage.getItem(key);

        if (offlineData !== null) {
            this.setState({ loading: false, data: JSON.parse(offlineData) })
        }


        firebase.database().ref("users").child(this.currentUser.uid).child("regionID").once("value").then(s => {
            firebase.database().ref("regions").child(s.val()).once("value").then(r => {
                let region = r.val();
                this.setState({ region: region });
                this.regionDataRef.child(parseInt(region.regCode) - 1).child("popdev").child(indicatorPath).on("value", d => {
                    localStorage.setItem(key, JSON.stringify(d.val()))
                    this.setState({ data: d.val(), loading: false })
                })
            })
        });
    }

    render() {
        const { data, loading, region } = this.state;
        return (
            <div>
                <Form loading={loading}>
                    <Table celled striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Indicators</Table.HeaderCell>
                                {/* <Table.HeaderCell textAlign="center">Source</Table.HeaderCell> */}
                                {/* <Table.HeaderCell textAlign="center">Validation</Table.HeaderCell> */}
                                {/* <Table.HeaderCell textAlign="center">Age</Table.HeaderCell>
                                <Table.HeaderCell textAlign="center">Attendees</Table.HeaderCell>
                                <Table.HeaderCell textAlign="center">Value</Table.HeaderCell>
                                <Table.HeaderCell>Remarks</Table.HeaderCell> */}
                                <Table.HeaderCell textAlign="center">Action</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {data !== [] && data!==null && Object.keys(data).map(key => {
                                return (
                                    <Table.Row key={uuidv4()}>
                                        <Table.Cell>
                                            {data[key].name}
                                        </Table.Cell>
                                        {/* <Table.Cell collapsing>
                                            {sourceOptions[data[key].source].text}
                                        </Table.Cell>
                                        <Table.Cell collapsing textAlign='center'>{data[key].age}</Table.Cell> */}
                                        {/* <Table.Cell collapsing textAlign='center'>{data[key].validation}</Table.Cell> */}
                                        {/* <Table.Cell collapsing textAlign='center'>
                                            {attendeesOptions[data[key].attendees].text}
                                        </Table.Cell>
                                        <Table.Cell collapsing textAlign='center'>{data[key].value}</Table.Cell>
                                        <Table.Cell collapsing textAlign='left'>{data[key].remarks}</Table.Cell> */}
                                        <Table.HeaderCell textAlign="center" collapsing>
                                        <EditIndicator indicator={data[key].name} path={"data/regions/" + (parseInt(region.regCode) - 1) + "/popdev/"+indicatorPath+"/" + key} />
                                        </Table.HeaderCell>
                                    </Table.Row>
                                )
                            })}
                        </Table.Body>
                    </Table>
                </Form>
            </div>
        )
    }
}
