import React, { Component } from 'react';
import {
    Button,
    Icon,
    Progress,
    Header,
    Menu,
    Container,
    Breadcrumb
} from 'semantic-ui-react';
import * as firebase from 'firebase';
import _ from 'lodash';
import { Doughnut, Bar } from 'react-chartjs-2';

import {
    Link,
    Redirect
} from 'react-router-dom';

let regions = ['I', 'II', 'III', 'IV-A', 'IV-B', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'ARMM', 'Caraga', 'CAR', 'NCR']

export default class NationalReportsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routeData: '/',
            redirect: false,
            demand: null,
            distribution: null,
            unmetReferred: null,
            region: {
                regCode: 0
            },
            loading: true,
            percent: 0,
            active_region: this.props.match.params.region
        }
        
        this.regionDataRef = firebase.database().ref("data").child("regions");
        this.currentUser = firebase.auth().currentUser;
    }

    componentDidMount() {
        const app = this;
        let active_region = this.props.match.params.region;
        app.setState({active_region:active_region});

        let regionBucket = [regions[this.props.match.params.region]];
        let demandSet1 = [];
        let demandSet2 = [];
        let maleSet = [];
        let femaleSet = [];
        let coupleSet = [];
        let unmetSet = [];
        let referredSet = [];

        app.regionDataRef.child(active_region).on("value", s => {
            let data = s.val();
            if (data !== null) {
                console.log(data);
                demandSet1.push(data.demand.target);
                demandSet2.push(
                    parseInt(data.demand.reached.couple) +
                    parseInt(data.demand.reached.male) +
                    parseInt(data.demand.reached.female)
                );
                unmetSet.push(data.demand.unmet);
                referredSet.push(data.demand.referred);
                maleSet.push(parseInt(data.demand.reached.male));
                femaleSet.push(parseInt(data.demand.reached.female));
                coupleSet.push(parseInt(data.demand.reached.couple));

                app.setState({
                    loading:false,
                    percent:100,
                    demand: {
                        labels: regionBucket,
                        datasets: [
                            {
                                label: 'Targetted No. of Couples/ Individuals',
                                backgroundColor: '#CDE0CD',
                                borderColor: '#75A975',
                                borderWidth: 1,
                                data: demandSet1
                            },
                            {
                                label: 'Total No. of Couples/ Individuals reached',
                                backgroundColor: '#75A975',
                                borderColor: '#75A975',
                                borderWidth: 1,
                                data: demandSet2
                            }
                        ]
                    }, distribution: {
                        labels: [
                            'Male',
                            'Females',
                            'Couples'
                        ],
                        datasets: [{
                            data: [_.sum(maleSet), _.sum(femaleSet), _.sum(coupleSet)],
                            backgroundColor: [
                                '#569456',
                                '#b8e7b8',
                                '#75A975'
                            ]
                        }]
                    }, unmetReferred: {
                        labels: regionBucket,
                        datasets: [
                            {
                                label: 'Couples/Individuals Reached',
                                backgroundColor: '#eaf6ea',
                                borderColor: '#75A975',
                                borderWidth: 1,
                                data: demandSet2
                            },
                            {
                                label: 'Unmet Need Identified',
                                backgroundColor: '#CDE0CD',
                                borderColor: '#75A975',
                                borderWidth: 1,
                                data: unmetSet
                            },
                            {
                                label: 'Referred and served',
                                backgroundColor: '#75A975',
                                borderColor: '#75A975',
                                borderWidth: 1,
                                data: referredSet
                            }
                        ]
                    }
                })
            }
        })
        
    }

    printDoc = () => {
        let data = {
            demand_chart: this.demand_chart.chartInstance.canvas.toDataURL(),
            unmetReferred_chart: this.unmetReferred_chart.chartInstance.canvas.toDataURL(),
            distribution_chart: this.distribution_chart.chartInstance.canvas.toDataURL()
        }
        localStorage.setItem('charts', JSON.stringify(data));
        this.setState({ routeData: null, redirect: true })
    }
    render() {
        const { demand, distribution, unmetReferred, percent, loading } = this.state;
        if (this.state.redirect) {
            return <Redirect push to={`/dashboard/doc/print/${this.state.routeData}`} />;
        }
        return (
            <div>
                <Progress color="green" autoSuccess active percent={percent} disabled={!loading} size='tiny' attached="top"/>      
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header>
                            <Header.Content>
                                <Breadcrumb>
                                    <Breadcrumb.Section as={Link} to="/dashboard/analytics">Generate Reports</Breadcrumb.Section>
                                    <Breadcrumb.Divider />
                                    <Breadcrumb.Section active>Regional {regions[this.props.match.params.region]} Data</Breadcrumb.Section>
                                </Breadcrumb>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Button basic icon labelPosition='left' onClick={this.printDoc}>
                                <Icon name='print' />
                                Print
                            </Button>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container textAlign="left">
                    <Header content="Demand Generation" />
                    {demand !== null && <Bar
                        ref={chart => this.demand_chart = chart}
                        data={demand}
                        width={100}
                        height={30}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />}
                    <Header content="Couples/Individuals Reached vs Identified Unmet Need vs Referred/Served" />
                    {unmetReferred !== null && <Bar
                        ref={chart => this.unmetReferred_chart = chart}
                        data={unmetReferred}
                        width={100}
                        height={50}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />}
                    <Header content="Distribution of Attendees" />
                    {distribution !== null && <Doughnut
                        ref={chart => this.distribution_chart = chart}
                        height={100}
                        options={{
                            maintainAspectRatio: true
                        }}
                        data={distribution} />}                    
                </Container>
            </div>
        )
    }
}