import React, { Component } from 'react';
import {
    Button,
    Icon,
    Header,
    Menu,
    Container,
    Breadcrumb,
    Divider,
    Table
} from 'semantic-ui-react';
import * as firebase from 'firebase';
import _ from 'lodash';
import async from 'async';
import { Doughnut, HorizontalBar } from 'react-chartjs-2';

import {
    Link,
    Redirect
} from 'react-router-dom';
const uuidv4 = require('uuid/v4');
let regions = ['I', 'II', 'III', 'IV-A', 'IV-B', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'ARMM', 'Caraga', 'CAR', 'NCR']

export default class CustomReportsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routeData: '/',
            redirect: false,
            data: null,
            chart_data: null,
            test_string: [],            
        }
    }

    componentDidMount() {
        const app = this
        let table_data = []
        var data = JSON.parse(localStorage.getItem('custom_report'));
        var processed_data = []
        var dataset = []
        var labels = []
        if (data != []) {
            async.forEachOf(data, function (value, key, callback) {
                let region = "Region "+ regions[value.region]
                labels.push(region);
                table_data.push(
                    <div key={uuidv4()}>
                        <Header color="green" as="h1">
                            {region}
                        </Header>
                        {Object.keys(value.data).map(i => (
                            <div>
                                <Header key={uuidv4()} content={_.capitalize(i)} />
                                {Object.keys(value.data[i]).map(ia => (
                                    <div>
                                        <Table celled striped>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell colSpan='2'>{_.startCase(ia)}</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>

                                                {Object.keys(value.data[i][ia]).map(ib => {
                                                    let val = value.data[i][ia][ib].value
                                                    let title = value.data[i][ia][ib].name
                                                    dataset.push({
                                                        label: title,                                                        
                                                        data: val
                                                    })
                                                    return (
                                                        <Table.Row>
                                                            <Table.Cell collapsing>
                                                                <Icon name='database' /> {title}
                                                            </Table.Cell>
                                                            <Table.Cell collapsing>{val}</Table.Cell>
                                                        </Table.Row>
                                                    )
                                                })}
                                            </Table.Body>
                                        </Table>
                                        <br />
                                    </div>
                                ))}
                            </div>
                        ))}
                        <br />
                    </div>
                );
                callback();
            }, function (err) {
                if (err) {
                    console.error(err.message);
                } else {
                    // console.log(labels);
                    let sorted_values = _.mapValues(_.groupBy(dataset,'label'))
                    let sorted_dataset = []                           
                    Object.keys(sorted_values).map(i=>{
                        sorted_dataset.push({
                            label: i,
                            data: _.map(sorted_values[i],'data'),
                            backgroundColor: '#eaf6ea',
                            borderColor: '#75A975',
                            borderWidth: 1,
                        })
                    })
                    console.log(sorted_dataset)
                    let chart_data = {
                        labels:labels,
                        datasets:sorted_dataset,                        
                    }
                    app.setState({ test_string: table_data, chart_data: chart_data });
                    console.log(chart_data)
                }
            })
        }
    }

    printDoc = () => {
        let data = {
            data_chart: this.data_chart.chartInstance.canvas.toDataURL(),
        }
        localStorage.setItem('charts', JSON.stringify(data));
        this.setState({ routeData: null, redirect: true })
    }
    render() {
        const { data, chart_data } = this.state;
        if (this.state.redirect) {
            return <Redirect push to={`/dashboard/doc/print/${this.state.routeData}`} />;
        }
        return (
            <div>
                <Menu pointing secondary>
                    <Menu.Item>
                        <Header>
                            <Header.Content>
                                <Breadcrumb>
                                    <Breadcrumb.Section as={Link} to="/dashboard/analytics">Generate Reports</Breadcrumb.Section>
                                    <Breadcrumb.Divider />
                                    <Breadcrumb.Section active>Custom Report Data</Breadcrumb.Section>
                                </Breadcrumb>
                            </Header.Content>
                        </Header>
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Button disabled basic icon labelPosition='left' onClick={this.printDoc}>
                                <Icon name='print' />
                                Print
                            </Button>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container>
                    <Header content="Chart Data" textAlign="left"/>
                    {chart_data!==null&&<HorizontalBar
                        ref={chart => this.custom_chart = chart}
                        data={chart_data}
                        width={100}
                        height={chart_data.datasets.length*7}
                        options={{
                            maintainAspectRatio: true
                        }}
                    />}
                </Container>
                <Container textAlign="left">
                    {/* {data!==null&&<p>{JSON.stringify(data)}</p>} */}
                    {this.state.test_string.map(i => i)}
                </Container>
            </div>
        )
    }
}