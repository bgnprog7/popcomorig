import React, { Component } from 'react';
import {Input, Button} from 'semantic-ui-react';
import ReactFireMixin from "reactfire";
import * as firebase from 'firebase';
export default class CollapsibleTextInput extends Component {
    mixins: [ReactFireMixin]
    constructor(props){
        super(props);
        this.state = {
            toggled: false,
            toggleEditIcon:false,
            text: this.props.text
        }
    }

    handleChange = (e, { value }) => {    
        this.setState({ text:value});    
    };

    handleBlur = (e) => {        
        this.props.firebaseRef.update({[this.props.childKey]:this.state.text}).then(()=>{
            this.toggleComponent()
        })
    };

    // componentDidMount(){
    //     this.setState({text:this.props.text})
    // }

    toggleComponent = ()=> {
        this.setState({toggled:!this.state.toggled});
        if(this.state.toggled==false){
            this.setState({toggleEditIcon:false})
        }
    }

    toggleEditIcon = ()=>{
        this.setState({toggleEditIcon: !this.state.toggleEditIcon})
    }

    render(){
        const {text, toggled, toggleEditIcon} = this.state;
        return(
            <div>
                {toggled?<Input fluid {...this.props} value={text} onChange={this.handleChange} onClose={this.toggleComponent} onBlur={this.handleBlur}/>:                         
                    <Button icon={toggleEditIcon?'pencil':''} style={{textAlign: 'left', backgroundColor:'transparent'}} compact onClick={this.toggleComponent} onMouseEnter={this.toggleEditIcon} onMouseLeave={this.toggleEditIcon} content={text} />        
                }
            </div>
        )
    }

}