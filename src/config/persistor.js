import storage from 'redux-persist/es/storage';
export function persistConfig(){
    return {
        key: 'root',
        storage,
        throttle: 1000
    }
}