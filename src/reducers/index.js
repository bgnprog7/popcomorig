
import region from './regionReducer';
import roles from './rolesReducer';
export default {
    region,
    roles,
}