import * as types from '../types/'
// this should be done on the cloud
const region = (state ={
    value: '',
    selection: [
        { key: 'ncr', value: 'ncr', text: 'National Capital Region (NCR)' },
        { key: 'r01', value: 'r01', text: 'Ilocos Region' },
        { key: 'car', value: 'car', text: 'Cordillera Administrative Region' },
        { key: 'r02', value: 'r02', text: 'Cagayan Valley' },
        { key: 'r03', value: 'r03', text: 'Central Luzon' },
        { key: 'r04', value: 'r04', text: 'CALABARZON' },
        { key: 'mimaropa', value: 'mimaropa', text: 'Southwestern Tagalog Region' },
        { key: 'r05', value: 'r05', text: 'Bicol Region' },
        { key: 'r06', value: 'r06', text: 'Western Visayas' },
        { key: 'r07', value: 'r07', text: 'Central Visayas' },
        { key: 'r08', value: 'r08', text: 'Eastern Visayas' },
        { key: 'r09', value: 'r09', text: 'Zamboanga Peninsula' },
        { key: 'r10', value: 'r10', text: 'Northern Mindanao' },
        { key: 'r11', value: 'r11', text: 'Davao Region' },
        { key: 'r12', value: 'r12', text: 'SOCCSKSARGEN' },
        { key: 'r16', value: 'r16', text: 'Caraga Region' },
        { key: 'armm', value: 'armm', text: 'Autonomous Region in Muslim Mindanao' },
    ]
}, action) => {
    switch(action.type){
        case types.SET_REGION: {
            return {...state, value: action.payload}         
        }
        default: return {...state}
    }
}

export default region;