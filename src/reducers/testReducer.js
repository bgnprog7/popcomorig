import * as types from '../types/'
// this should be done on the cloud
const test = (state ={
    test: null
}, action) => {
    switch(action.type){
        case types.TEST: {
            return {...state, test: action.payload}         
        }
        default: return {...state}
    }
}

export default test;
