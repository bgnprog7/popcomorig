import * as types from '../types/'
// this should be done on the cloud
const roles = (state ={
    activeRole: 'encoder', // admin | econder
}, action) => {
    switch(action.type){
        case types.SET_ROLE: {
            return {...state, activeRole: action.payload}         
        }
        default: return {...state}
    }
}

export default roles;
