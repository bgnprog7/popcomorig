import * as types from '../types/'

export function setRegion(value) {
    return function (dispatch) {
        dispatch({ type: types.SET_REGION, payload: value });
    }
}

export function setRole(value) {
    return function (dispatch) {
        dispatch({ type: types.SET_ROLE, payload: value });
    }
}

export function rehydrate(value) {
    return function (dispatch) {
        dispatch({ type: types.REHYDRATE, payload: value });
    }
}