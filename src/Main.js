import React, {Component} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Async from 'react-code-splitting';
import ReactFireMixin from "reactfire";
import * as firebase from 'firebase';

const firebaseConfig = require('./config/firebase');

const Home = () => <Async load={import('./pages/Home')} />
const Login = () => <Async load={import('./pages/Login')} />
const Dashboard = () => <Async load={import('./pages/protected/Dashboard')} />
// const Charts = () => <Async load={import('./pages/protected/Charts')} />


firebase.initializeApp(firebaseConfig);

const PrivateRoute = ({component: Component, authed, ...rest}) => {
    return (
      <Route
        {...rest}
        render={(props) => authed === true
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
      />
    )
  }
  
const PublicRoute = ({component: Component, authed, ...rest}) => {
    return (
      <Route
        {...rest}
        render={(props) => authed === false
          ? <Component {...props} />
          : <Redirect to='/dashboard' />}
      />
    )
  }
  
export default class Main extends Component {
  mixins : [ReactFireMixin]
  constructor(props){
    super(props);
    this.state = {
      authed: false,
      loading: true
    }
  }

  componentDidMount () {
    this.removeListener = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          authed: true,
          loading: false,
        })
      } else {
        this.setState({
          authed: false,
          loading: false
        })
      }
    })
}

componentWillUnmount () {
  this.removeListener()
}

  render(){
    return(
      <main>
        <Switch>
            <Route exact path="/" component={Home} />
            <PublicRoute authed={this.state.authed} path='/login' component={Login} />
            <PrivateRoute authed={this.state.authed} path='/dashboard' component={Dashboard} />
            <Route render={() => <h3>Page not found.</h3>} />
        </Switch>
      </main>
    )
  }

}

// const Main = () => (

// );

// export default Main;