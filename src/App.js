import React from 'react';
import Main from './Main'
import { Provider } from 'react-redux';
import './App.css';
import { PersistGate } from 'redux-persist/es/integration/react';
import { configureStore } from './store/configureStore';
const { persistor, store } = configureStore();

const App = () => (
  <Provider store={store}>

    <div className="App">
      <PersistGate
        loading={<div>Loading...</div>}
        persistor={persistor}>
        <Main />
      </PersistGate>
    </div>

  </Provider>

)

export default App;

