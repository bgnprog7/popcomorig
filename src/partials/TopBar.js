import React, { Component } from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
// import { withRouter } from 'react-router-dom';
import {
    Image,
    Menu,
    Icon,
    Dropdown
} from 'semantic-ui-react';

import { logout } from '../helpers/';

export default class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authed: false
        }
    }
    componentDidMount() {
        this.setState({ authed: this.props.authed })
    }
    render() {
        const { authed } = this.state;
        return (
            <Menu fixed='top' borderless inverted className="top-bar">
                
                    <Menu.Item as={Link} header to="/">
                        <Image src='/assets/img/brand-icon.png' className="brand-icon" />
                    </Menu.Item>
                    {/*
                    <Menu.Item as={Link} to="/" name='HOME'>
                        <span>HOME</span>
                    </Menu.Item>
                     <Menu.Item as={Link} to="/rpfp" name='RPFP'>
                        <span>RPFP</span>
                    </Menu.Item>
                    <Menu.Item as={Link} to="/ahd" name='AHD'>
                        <span>AHD</span>
                    </Menu.Item>
                    <Menu.Item as={Link} to="/popdev" name='POPDEV'>
                        <span>POPDEV</span>
                    </Menu.Item>
                    <Menu.Item as={Link} to="/reports" name='REPORTS'>
                        <span>REPORTS</span>
                    </Menu.Item> */}

                    <Menu.Menu position='right'>

                        {!authed ?
                            <Dropdown text="User" name="ellipsis horizontal" pointing className='link item'>
                                <Dropdown.Menu>                                    
                                    <Dropdown.Item as={Link} to="/login">
                                        <Icon name='user' />Login</Dropdown.Item>                                    
                                </Dropdown.Menu>
                            </Dropdown>
                            :
                            <Dropdown text="Account" name="ellipsis horizontal" pointing className='link item'>
                                <Dropdown.Menu>
                                    <Dropdown.Header className="dropdown-header">POPCOM Editor</Dropdown.Header>
                                    <Dropdown.Item disabled><Icon name="help circle" />Help</Dropdown.Item>
                                    <Dropdown.Item onClick={() => logout()}><Icon name="sign out" />Log out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        }
                    </Menu.Menu>
              
            </Menu>
        )
    }
}