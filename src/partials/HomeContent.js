import React, { Component } from 'react';
import '../App.css';
import { Fade } from 'react-reveal';
import {
    Container,
    Grid,
    Header,
    Image
} from 'semantic-ui-react';

class HomeContent extends Component {
    render() {
        return (
            <div>
                <Container className="home-contents">
                <div style={{padding: '2em'}}>
                    <Fade>                        
                        <Header as="h1" content="Core Features" />
                        <Grid relaxed padded textAlign="center" stackable>
                            <Grid.Column width={4}>
                                <Image src='/assets/img/dashboard-icon.png' size="small" centered />
                                <Header as="h4" content="Dashboard" className="home-grid-header" />
                                <p>Gives you a quick overview of the system.</p>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Image src='/assets/img/reports-icon.png' size="small" centered />
                                <Header as="h4" content="Reports" className="home-grid-header" />
                                <p>Manage you population reports online</p>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Image src='/assets/img/analytics-icon.png' size="small" centered />
                                <Header as="h4" content="Analytics" className="home-grid-header" />
                                <p>Gain insights on your current data inputs</p>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Image src='/assets/img/pdf-icon.png' size="small" centered />
                                <Header as="h4" content="PDF On-the-Go" className="home-grid-header" />
                                <p>Reports can easily be exported to PDF</p>
                            </Grid.Column>
                        </Grid>
                    </Fade>
                        </div>
                </Container>
            </div>
        )
    }
}

export default HomeContent