import React, { Component } from 'react';
import '../App.css';
import {
    Container,
    Button,
    Header,
    Image
} from 'semantic-ui-react';
import { Fade } from 'react-reveal';
import { Link } from 'react-router-dom';

class HomeHeader extends Component {

    render() {
        return (
            <div className="home-header">
                <Fade>
                    <Container className="home-header-container">
                    <Image src="assets/img/logo.png" className="home-header-logo"/>                              
                    <Header className="home-header-text-jumbo" inverted size="huge" textAlign="left">
                        Welcome
                    </Header>
                    <p className="home-header-text home-header-text--width-limited">
                    This is a data management system for the Commission on Population of the Philippines
                    </p>
                    <Button as={Link} to="/login" secondary size="huge" >Get Started</Button>
                    </Container>
                </Fade>
            </div>
        )
    }
}

export default HomeHeader;