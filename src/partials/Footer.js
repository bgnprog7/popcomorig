import React, {Component} from 'react';
import '../App.css';
// import {Link} from 'react-router-dom';
// import {withRouter} from 'react-router-dom';
import {
    Header,
    Container
  } from 'semantic-ui-react';

  class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <Container>
                    <Header as="h4" content="POPCOM 2017" className="footer-header" textAlign="left"/>
                    <p style={{color: '#FFF', fontSize: 'small', textAlign: 'left'}}>Copyright &copy; 2017. Commission on Population of the Philippines</p>
                </Container>
            </div>
        )
        }
  }

  export default Footer;