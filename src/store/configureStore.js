import {applyMiddleware, createStore, compose} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { persistStore, persistCombineReducers } from 'redux-persist';
import reducers from '../reducers/';
import localForage from 'localforage';
const logger = createLogger();

const enhancer = compose (
    applyMiddleware(
        promise(),
        thunk,
        logger
    ),
)

const persistConfig = {
    key: 'root',
    storage: localForage,
    throttle: 1000,
    debug: true
}
 
const reducer = persistCombineReducers(persistConfig, reducers);

export function configureStore(initialState){

    let store = createStore(reducer, initialState, enhancer);
    let persistor = persistStore(store);
    return {persistor, store}
}
