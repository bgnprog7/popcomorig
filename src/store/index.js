// import initSubscriber from 'redux-subscriber';
import {configureStore} from './configureStore';

const store = configureStore({});

// const subscribe = initSubscriber(store);

export default store;