import React from 'react';
import {Container} from 'semantic-ui-react';
import LoginForm from '../components/LoginForm';
import '../App.css';

const Login = () => (
    <div className="login-background">
        <Container className="contents full-height contents-login">
        <div style={{padding: '2em'}}>
        <LoginForm /> 
        </div>
            
        </Container>
    </div>
);

export default Login;