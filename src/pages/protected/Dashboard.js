import React, {Component} from 'react';
import '../../App.css';
import ClientDashboard from './ClientDashboard'
import AdminDashboard from './AdminDashboard'
import * as firebase from 'firebase';

class Dashboard extends Component {
  constructor(props){
    super(props)
    this.currentUser = firebase.auth().currentUser
  }

  componentDidMount() {

  }

  render(){
    // NOTE: Deter mine if admin
    return (
      <div>
      {this.currentUser.uid==='lrk70mX6qSXxYaivShHrpFBaHv22'?<AdminDashboard />:<ClientDashboard />}
      </div>
    )
  }
}

const Content = () => (
    <Dashboard />
)

export default Content;
