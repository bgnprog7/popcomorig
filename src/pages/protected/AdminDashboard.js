import React, { Component } from 'react';
import { Message, Menu, List, Image, Icon, Header, Grid, Divider, Button, Container, Checkbox } from 'semantic-ui-react'
import About from '../../components/About';
import Users from '../../components/Users';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import ReportsPreview from '../../components/ReportsPreview';
import RegionCards from '../../components/RegionCards';
import '../../App.css';
import { logout } from '../../helpers/';
import PDF from '../../components/PDF';
import NationalReportsView from '../../components/NationalReportsView';
import AHYDIndicatorContent from '../../components/INDICATORS/AHYD/Contents';
import POPDEVIndicatorContent from '../../components/INDICATORS/POPDEV/Contents';
import RPFPIndicatorContent from '../../components/INDICATORS/RPFP/Contents';
import RegionalReportsView from '../../components/RegionalReportsView';
import AddIndicator from '../../components/AddIndicator';
import moment from 'moment';
import CustomReports from '../../components/CustomReports';
import CustomReportsView from '../../components/CustomReportsView';
import DataExporter from '../../components/DataExporter';
const JSONViewer = require('react-json-viewer');
const AdminDashboard = () => (
  <Router>
    <div style={{ display: 'flex', flexDirection: 'column' }}>

      <div className="sidenav">
        <Image style={{ margin: '20px' }} src='/assets/img/brand-icon.png' className="brand-icon" />
        <Divider />
        <List>
          <List.Item className="dashboard-menu-item-header" to="/dashboard/admin">
            <Icon name='user' className="dashboard-menu-icon" />
            Admin
          </List.Item>
          <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/">
            <Icon name='home' className="dashboard-menu-icon" />
            Home
          </List.Item>
          <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/indicators">
            <Icon name='industry' className="dashboard-menu-icon" />
            Manage Indicators
          </List.Item>
          <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/users">
            <Icon name='users' className="dashboard-menu-icon" />
            Manage Users
          </List.Item>
          <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/analytics">
            <Icon name='bar chart' className="dashboard-menu-icon" />
            Generate Reports
          </List.Item>
          <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/export">
            <Icon name='external' className="dashboard-menu-icon" />
            Export and Backup
          </List.Item>
          <List.Item className="dashboard-menu-item" as={Link} to="/dashboard/help">
            <Icon name='help circle' className="dashboard-menu-icon" />
            About
          </List.Item>
          <Divider />
          <List.Item className="dashboard-menu-item" onClick={() => logout()}>
            <Icon name='sign out' className="dashboard-menu-icon" />
            Log Out
          </List.Item>
        </List>
      </div>
      <div className="contents">
        <Route exact path="/dashboard" component={Home} />
        <Route path="/dashboard/users" component={USERS} />
        <Route path="/dashboard/analytics" component={Reports} />
        <Route path="/dashboard/indicators" component={Indicators} />
        <Route path="/dashboard/export" component={Export} />
        <Route path="/dashboard/customreportview" component={CustomReportView} />
        <Route path="/dashboard/help" component={HELP} />
        <Route path="/dashboard/doc/print/:value" component={PDFPrint} />
        <Route path="/dashboard/doc/download" component={PDFDownload} />
        <Route path="/dashboard/national_data" component={NationalData} />
        <Route path="/dashboard/dummy_data" component={DummyData} />
        <Route path="/dashboard/regional_data/:region" component={RegionalData} />
      </div>

    </div>
  </Router>
);

const PDFPrint = (props) => (
  <PDF action="print" documentData={props.match.params.value} />
)

const PDFDownload = () => (
  <PDF action="download" />
)

class CustomReportViewJSON extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null
    }
  }
  componentDidMount() {
    var data = JSON.parse(localStorage.getItem('custom_report'));    
    if(data!=[]){
      this.setState({ data: data })
    }
  }
  render() {
    const { data } = this.state;
    return (
      <div>
        <Menu pointing secondary>
          <Menu.Item>
            <Header textAlign="left">
              <Header.Content>
                Custom Report
            <Header.Subheader>
                  Demo Table
            </Header.Subheader>
              </Header.Content>
            </Header>
          </Menu.Item>
        </Menu>
        <Container textAlign="left">
          {data !== null && <JSONViewer json={data}></JSONViewer>}
        </Container>
      </div>
    )
  }
}

const CustomReportView = () => (
  <CustomReportsView />
)

const USERS = () => (
  <div>
    <Users />
  </div>
)

const Indicators = () => (
  <div>

    <Menu pointing secondary>
      <Menu.Item>
        <Header textAlign="left">
          <Header.Content>
            Indicators
            <Header.Subheader>
              Manage POPCOM Indicators
            </Header.Subheader>
          </Header.Content>
        </Header>
      </Menu.Item>

      <Menu.Menu position='right'>
        <AddIndicator />
      </Menu.Menu>
    </Menu>
    <Container textAlign="left">
      <p className="text--left">Lets you manage indicator records.</p>
      {/* <Message warning>
      <Message.Header>Alpha Version!</Message.Header>
      <p>This feature is currently disabled in alpha stage.</p>
    </Message> */}
      <RPFPIndicatorContent />
      <Divider />
      <POPDEVIndicatorContent />
      <Divider />
      <AHYDIndicatorContent />
    </Container>
  </div>
)

const NationalData = () => (
  <div>
    <NationalReportsView />
  </div>
)

const DummyData = () => (
  <div>
    <ReportsPreview />
  </div>
)

const RegionalData = (props) => (
  <div>
    <RegionalReportsView {...props} />
  </div>
)

const Reports = () => (
  <div>
    <Menu pointing secondary>
      <Menu.Item>
        <Header textAlign="left">
          <Header.Content>
            Reports
<Header.Subheader>
              Gain insights on the current data entries.
</Header.Subheader>
          </Header.Content>
        </Header>
      </Menu.Item>

      <Menu.Menu position='right'>

      </Menu.Menu>
    </Menu>
    <Container textAlign="left">
      <Header color="green" as="h3" content={"Data as of " + moment().format("MMMM Do YYYY")} />
      <Divider />
      <RegionCards />
      <Divider />
      <CustomReports />
    </Container>
  </div>
)

const Export = () => (
  <div>
    <Menu pointing secondary>
      <Menu.Item>
        <Header textAlign="left">
          <Header.Content>
            Export
<Header.Subheader>
              Tools and utilities for data backup.
</Header.Subheader>
          </Header.Content>
        </Header>
      </Menu.Item>

      <Menu.Menu position='right'>

      </Menu.Menu>
    </Menu>
    <Container textAlign="left">
    <p>Available Tools</p>
    <DataExporter />
    </Container>
  </div>
)

const Home = () => (
  <div>
    <Menu pointing secondary>
      <Menu.Item>
        <Header textAlign="left">
          <Header.Content>
            POPCOM Admin
<Header.Subheader>
              Welcome and Mabuhay!
</Header.Subheader>
          </Header.Content>
        </Header>
      </Menu.Item>

      <Menu.Menu position='right'>

      </Menu.Menu>
    </Menu>
    <Container fluid style={{ padding: '20px' }}>
      <Header as="h1" content="Welcome Admin!" />
      <p>Please select a module on the sidebar to begin.</p>
      <Grid relaxed padded textAlign="center" stackable>
        <Grid.Column width={4}>
          <Image src='/assets/img/dashboard-icon.png' size="small" centered />
          <Header as="h4" content="Dashboard" className="home-grid-header" />
          <p>Gives you a quick overview of the system.</p>
        </Grid.Column>
        <Grid.Column width={4}>
          <Image src='/assets/img/reports-icon.png' size="small" centered />
          <Header as="h4" content="Reports" className="home-grid-header" />
          <p>Manage you population reports online</p>
        </Grid.Column>
        <Grid.Column width={4}>
          <Image src='/assets/img/analytics-icon.png' size="small" centered />
          <Header as="h4" content="Analytics" className="home-grid-header" />
          <p>Gain insights on your current data inputs</p>
        </Grid.Column>
        <Grid.Column width={4}>
          <Image src='/assets/img/pdf-icon.png' size="small" centered />
          <Header as="h4" content="PDF On-the-Go" className="home-grid-header" />
          <p>Reports can easily be exported to PDF</p>
        </Grid.Column>
      </Grid>
      <Divider />
    </Container>
  </div>
)

const HELP = () => (
  <About />
)

export default AdminDashboard;
