import React from 'react';
import {Container, Divider, Menu, List, Image, Icon, Header, Grid} from 'semantic-ui-react'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import RPFPContents from '../../components/RPFP/Contents';
import POPDEVContents from '../../components/POPDEV/Contents';
import AHYDContents from '../../components/AHYD/Contents';
import DemandGeneration from '../../components/DemandGeneration';
import About from '../../components/About';
import '../../App.css';
import { logout } from '../../helpers';

const ClientDashboard = () => (
    <Router>
        <div style={{ display: 'flex', flexDirection: 'column' }}>

            <div className="sidenav">
                <Image style={{ margin: '20px' }} src='/assets/img/brand-icon.png' className="brand-icon" />
                <Divider />
                <List>
                    <List.Item className="dashboard-menu-item-header">
                        <Icon name='pencil' className="dashboard-menu-icon" />
                        Data Entry
                    </List.Item>
                    <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/rpfp">
                        <Icon name='database' className="dashboard-menu-icon" />
                        RPFP
                    </List.Item>
                    <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/popdev">
                        <Icon name='database' className="dashboard-menu-icon" />
                        POPDEV
                    </List.Item>
                    <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/ahyd">
                        <Icon name='database' className="dashboard-menu-icon" />
                        AHD
                    </List.Item>
                    <List.Item className="dashboard-menu-item-submenu" as={Link} to="/dashboard/demand">
                        <Icon name='bar graph' className="dashboard-menu-icon" />
                        Demand Generation
                    </List.Item>
                    <List.Item className="dashboard-menu-item" as={Link} to="/dashboard/help">
                        <Icon name='help circle' className="dashboard-menu-icon" />
                        About
                    </List.Item>
                    <Divider />
                    <List.Item className="dashboard-menu-item" onClick={() => logout()}>
                        <Icon name='sign out' className="dashboard-menu-icon" />
                        Sign Out
                    </List.Item>
                </List>
            </div>
            <div className="contents">
                <Route exact path="/dashboard/" component={HOME} />
                <Route path="/dashboard/popdev" component={POPDEV} />
                <Route path="/dashboard/rpfp" component={RPFP} />
                <Route path="/dashboard/ahyd" component={AHYD} />
                <Route path="/dashboard/demand" component={DEMAND} />
                <Route path="/dashboard/help" component={HELP} />
            </div>
        </div>
    </Router>
);


const HOME = () => (
    <div>
        <Menu pointing secondary>
            <Menu.Item>
                <Header textAlign="left">
                    <Header.Content>
                        POPCOM Encoder
<Header.Subheader>
                            Welcome and Mabuhay!
</Header.Subheader>
                    </Header.Content>
                </Header>
            </Menu.Item>

            <Menu.Menu position='right'>

            </Menu.Menu>
        </Menu>
        <Container fluid style={{ padding: '20px' }}>
            <Header as="h1" content="Welcome Encoder!" />
            <p>Please select a module on the sidebar to begin.</p>
            <Grid relaxed padded textAlign="center" stackable>
                <Grid.Column width={4}>
                    <Image src='/assets/img/dashboard-icon.png' size="small" centered />
                    <Header as="h4" content="Dashboard" className="home-grid-header" />
                    <p>Gives you a quick overview of the system.</p>
                </Grid.Column>
                <Grid.Column width={4}>
                    <Image src='/assets/img/reports-icon.png' size="small" centered />
                    <Header as="h4" content="Reports" className="home-grid-header" />
                    <p>Manage you population reports online</p>
                </Grid.Column>
                <Grid.Column width={4}>
                    <Image src='/assets/img/analytics-icon.png' size="small" centered />
                    <Header as="h4" content="Analytics" className="home-grid-header" />
                    <p>Gain insights on your current data inputs</p>
                </Grid.Column>
                <Grid.Column width={4}>
                    <Image src='/assets/img/pdf-icon.png' size="small" centered />
                    <Header as="h4" content="PDF On-the-Go" className="home-grid-header" />
                    <p>Reports can easily be exported to PDF</p>
                </Grid.Column>
            </Grid>
            <Divider />
        </Container>
    </div>
)

const POPDEV = () => (
    <div>
        <POPDEVContents />
    </div>
)

const RPFP = () => (
    <div>
        <RPFPContents />
    </div>
)

const AHYD = () => (
    <div>
        <AHYDContents />
    </div>
)

const DEMAND = () => (
<div>
    <DemandGeneration />
</div>

)

const HELP = () => (
    <div>
       <About />
    </div>
)

export default ClientDashboard;
