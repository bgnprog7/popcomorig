import React from 'react';
import TopBar from '../partials/TopBar';
import HomeHeader from '../partials/HomeHeader';
import HomeContent from '../partials/HomeContent';
import Footer from '../partials/Footer';

import '../App.css';

const Home = () => (
    <div>
        <TopBar />
        <HomeHeader />
        <HomeContent />
        <Footer />
    </div>
);

export default Home;